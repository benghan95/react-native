import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { StackNavigator, DrawerNavigator, DrawerItems } from 'react-navigation';
import  { createStackNavigator, createDrawerNavigator }  from 'react-navigation';
import LoginStackNavigator from './src/components/navigation/LoginStackNavigator';
import CoreStackNavigator from './src/components/navigation/CoreStackNavigator';
import styled from "styled-components";
import { Scene, Router, TabNavigator, ActionConst } from 'react-native-router-flux';

import Landing from './src/components/screens/LandingScreen';
import Login from './src/components/screens/LoginScreen';
import Welcome from './src/components/screens/WelcomeScreen';
import Home from './src/components/screens/HomeScreen';
import Menu from './src/components/screens/MenuScreen';
import ProductDetails from './src/components/screens/ProductDetailsScreen';
import UploadMethod from './src/components/screens/UploadMethodScreen';
import QRCode from './src/components/screens/QRCodeScreen';
import ScanReceipt from './src/components/screens/ScanReceiptScreen';
import ManualUpload from './src/components/screens/ManualUploadScreen';
import UploadSuccess from './src/components/screens/UploadSuccessScreen';
import ManualUploadSuccess from './src/components/screens/ManualUploadSuccessScreen';
import ContactUs from './src/components/screens/ContactUsScreen';
import Terms from './src/components/screens/TermsScreen';
import PrivacyPolicy from './src/components/screens/PrivacyPolicyScreen';
import EditDetails from './src/components/screens/EditDetailsScreen';
import Admin from './src/components/screens/AdminScreen';
import AdminEdit from './src/components/screens/AdminEditScreen';

const DrawerContainer = styled.View`
  flex: 1;
`;

const AppContainer = styled.View`
  flex: 1;
`;

// const drawerRouteConfig = {
//   Login: {
//     screen: LoginStackNavigator,
//   },
//   Core: {
//     screen: CoreStackNavigator,
//   }
// };

// const drawerNavigatorConfig = {
// };

// const Stack = StackNavigator(
//   {
//     Login: {
//         screen: LoginStackNavigator,
//     },
//   },
//   {
//     initialRouteName: 'Login',
//     headerMode      : 'none',
//   },
// );

// const AppDrawer = DrawerNavigator(drawerRouteConfig, drawerNavigatorConfig);

export default class App extends Component {
  state = {
    fontLoaded: true,
  };

  // async componentDidMount() {
  //   await Font.loadAsync({
  //     'gotham-bold'  : require('./assets/fonts/Gotham-Bold.ttf'),
  //     'gotham-medium': require('./assets/fonts/Gotham-Medium.otf'),
  //     'gotham-book'  : require('./assets/fonts/Gotham-Book.otf'),
  //   })

  //   this.setState({ fontLoaded: true });
  // }

  render() {
    return (
      this.state.fontLoaded ? (
        <Router>
          <Scene key="root" hideNavBar={true}>
            {/* <Scene key="MainApp" hideNavBar={true}> */}
              <Scene key="Login" component={Login} type={ActionConst.RESET} panHandlers={null} />
              <Scene key="Welcome" component={Welcome} />
              <Scene key="Terms" component={Terms} />
              <Scene key="PrivacyPolicy" component={PrivacyPolicy} />
            {/* </Scene>
            <Scene key="MainApp" hideNavBar={true} initial={true}> */}
              <Scene key="Landing" component={Landing} initial={true} />
              <Scene key="Home" component={Home} type={ActionConst.RESET} panHandlers={null} />
              <Scene key="Menu" component={Menu} />
              <Scene key="ProductDetails" component={ProductDetails} />
              <Scene key="UploadMethod" component={UploadMethod} />
              <Scene key="QRCode" component={QRCode} />
              <Scene key="ScanReceipt" component={ScanReceipt} />
              <Scene key="ManualUpload" component={ManualUpload} />
              <Scene key="UploadSuccess" component={UploadSuccess} />
              <Scene key="ManualUploadSuccess" component={ManualUploadSuccess} />
              <Scene key="ContactUs" component={ContactUs} />
              <Scene key="EditDetails" component={EditDetails} />
              <Scene key="Admin" component={Admin} />
              <Scene key="AdminEdit" component={AdminEdit} />
            {/* </Scene> */}
          </Scene>
        </Router>
      ) : null
    );
  }

  // <AppContainer>
  //   <StatusBar hidden={true} />
  //   <CoreStackNavigator />
  // </AppContainer>
}