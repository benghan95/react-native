# Setup
1. run 'npm install' in the project folder
2. run 'cd ios && pod install' in the project folder (or go to 'ios' folder & run 'pod install')
3. Follow every steps below:

## Facebook
1. Download SDK @ https://origincache.facebook.com/developers/resources/?id=facebook-ios-sdk-current.zip
2. Unzip the archive to ~/Documents/FacebookSDK.

## React Native Swiper
1. Ensure that version stays at v1.5.13
2. Go to node_modules/react-native-swiper/src/index.js
3. Comment out line 465, 466, 467 and line 469
  ```javascript
  // if (Platform.OS !== 'ios') {               // line 465
  //   this.scrollView && this.scrollView[animated ? 'setPage' : 'setPageWithoutAnimation'](diff)       // line 466
  // } else {             // line 467
    this.scrollView && this.scrollView.scrollTo({ x, y, animated })
  // }            // line 469
  ```
4. Comment out line 624 and line 638
  ```javascript
  // if (Platform.OS === 'ios') {     // line 624
    return (
      <ScrollView ref={this.refScrollView}
        {...this.props}
        {...this.scrollViewPropOverrides()}
        contentContainerStyle={[styles.wrapperIOS, this.props.style]}
        contentOffset={this.state.offset}
        onScrollBeginDrag={this.onScrollBegin}
        onMomentumScrollEnd={this.onScrollEnd}
        onScrollEndDrag={this.onScrollEndDrag}
        style={this.props.scrollViewStyle}>
        {pages}
      </ScrollView>
    )
  // }             // line 638
  return (
    <ViewPagerAndroid ref={this.refScrollView}
      {...this.props}
      initialPage={this.props.loop ? this.state.index + 1 : this.state.index}
      onPageSelected={this.onScrollEnd}
      key={pages.length}
      style={[styles.wrapperAndroid, this.props.style]}>
      {pages}
    </ViewPagerAndroid>
  )
  ```

## React Native Maps
1. react-native-maps/lib/android/build.gradle
2. DEFAULT_GOOGLE_PLAY_SERVICES_VERSION = "15.0.1"