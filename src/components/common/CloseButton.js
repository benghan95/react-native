import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';

class CloseButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[{ position: 'absolute', top: 0, right: 5, zIndex: 80 }, this.props.style]}>
        <TouchableOpacity onPress = {this.props.onPress}>
          <View style={{ padding: 15 }}>
            <Image
              source = {this.props.blue ? require('./../../../assets/icons/default/blue/Close.png') : require('./../../../assets/icons/default/white/Close.png')}
              style  = {{ width: 20, height: 20, resizeMode: 'contain', }}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default CloseButton;