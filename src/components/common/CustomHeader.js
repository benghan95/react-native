import React, { Component } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from '../../constants/styles';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
  getStartedIcon: {
    ...Platform.select({
      ios: {
        marginTop  : -7,
        marginRight: 0,
      },
      android: {
        marginTop  : 0,
        marginRight: 0,
      },
    }),
  },
  pageTitle: {
    color     : '#fff',
    fontSize  : 18,
    fontFamily: 'Gotham-Bold',
  },
  getStartedText: {
    color     : '#ffffff',
    fontSize  : 24,
    textAlign : 'center',
    fontFamily: 'Gotham-Bold',
  },
  container: {
    flex           : 1,
    backgroundColor: '#4A90E2',
  },
  rightContainer: {
    flexDirection: 'row'
  },
  logoView: {
    flex          : 3,
    alignItems    : 'center',
    justifyContent: 'flex-end',
  },
  actionView: {
    flex          : 2,
    alignItems    : 'center',
    justifyContent: 'center',
  }
});

export class DefaultRightComponent extends Component {
  render() {
    return (
      <View style = { styles.rightContainer}>
        {/* <Icon
          name      = 'search'
          type      = 'feather'
          color     = '#fff'
          component = { TouchableOpacity }
          iconStyle = {{paddingVertical: 15, paddingHorizontal: 10}}
          // onPress        = { () => this.props.navigation.goBack() }
          size = {24}
        /> */}
        <TouchableOpacity
          onPress = { () => this.props.navigation.navigate('Menu') }
        >
          <View
            style = {{ paddingLeft: 10, paddingRight: 20}}
          >
            <Image
              source = {require('./../../../assets/icons/default/white/Menu.png')}
              style  = {{ width: 30, height: 30, resizeMode: 'contain', }}
            />
          </View>
        </TouchableOpacity>
        {/* <Icon
          name      = 'md-menu'
          type      = 'ionicon'
          color     = '#fff'
          component = { TouchableOpacity }
          iconStyle = {{paddingTop: 12, paddingBottom: 9, paddingLeft: 10, paddingRight: 25}}
          onPress   = { () => this.props.navigation.navigate('Menu') }
          size      = {30}
        /> */}
      </View>
    )
  }
}

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}


export default class CustomHeader extends Component {
  state = {
    isReady: false,
  };

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    const fontAssets = cacheFonts([FontAwesome.font, Feather.font]);

    await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    if (this.props.type == 'backOnly'){
      return (
        <Header
          statusBarProps       = {{ barStyle: 'light-content' }}
          outerContainerStyles = {{
            backgroundColor  : this.props.bgColor,
            borderBottomWidth: 0,
            paddingHorizontal: 0,
            paddingBottom    : 0,
          }}
          innerContainerStyles= {
            {alignItems: 'center'}
          }
          centerComponent = { this.props.pageTitle ? { text: this.props.pageTitle, style: styles.pageTitle } : null}
          leftComponent   = {
            <Icon
              name      = 'arrow-left'
              type      = 'feather'
              color     = '#fff'
              component = { TouchableOpacity }
              iconStyle = {{paddingVertical: 13, paddingHorizontal: 15}}
              onPress   = { () => Actions.pop() }
              size      = {28}
            />
          }
        />
      );
    }
    if (this.props.type == 'default'){
      return (
        <Header
          statusBarProps       = {{ barStyle: 'light-content' }}
          outerContainerStyles = {[this.props.outerContainerStyles, {
            backgroundColor  : this.props.bgColor,
            borderBottomWidth: 0,
            paddingHorizontal: 0,
            paddingBottom    : 0,
          }]}
          innerContainerStyles= {
            {alignItems: 'center',}
          }
          leftComponent = {
            this.props.leftComponent ? 
            this.props.uniqueLeftComponent ? this.props.uniqueLeftComponent: 
            <Icon
              name      = 'arrow-left'
              type      = 'feather'
              color     = '#fff'
              component = { TouchableOpacity }
              iconStyle = {{paddingVertical: 13, paddingHorizontal: 15}}
              onPress   = { () => this.props.navigation.goBack() }
              size      = {28}
            /> : null
          }
          rightComponent = {<DefaultRightComponent navigation={this.props.navigation}/>}
        />
      );
    }
  }
}