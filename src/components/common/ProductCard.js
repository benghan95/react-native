import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, Text, StyleSheet, View, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { categories } from '../../data/categories';
import moment from 'moment';

const CardContainer = styled.View`
  position: relative;
`;

const ProductContainer = styled.View`
  backgroundColor  : #fff;
  borderWidth      : 2.5;
  borderColor      : #E0E2E1;
  borderRadius     : 10;
  marginVertical   : 5;
  marginLeft       : 20;
  marginRight      : 20;
  paddingHorizontal: 10;
  paddingVertical  : 15;
  flexDirection    : row;
  justifyContent   : center;
  alignItems       : center;
`;

const ProgressContainer = styled.View`
  width         : 40%;
  alignItems    : center;
  justifyContent: center;
  position      : relative;
`;

const ProgressCategoryImage = styled.Image`
  alignSelf : center;
  width     : 40;
  height    : 40;
  position  : absolute;
  resizeMode: contain;
`;

const InfoContainer = styled.View`
  width      : 60%;
  paddingLeft: 10;
`;

const ItemTitle = styled.Text`
  color     : #4D4D4F;
  fontSize  : 18;
  fontFamily: 'Gotham-Bold';
`;

const ItemDuration = styled.Text`
  color     : #000000;
  opacity   : 0.5;
  fontSize  : 14;
  fontFamily: 'Gotham-Bold';
`;

const ErrorContainer = styled.View`
  position      : absolute;
  top           : 0;
  left          : 0;
  bottom        : 0;
  borderRadius  : 10;
  marginVertical: 5;
  marginLeft    : 20;
  marginRight   : 20;
`;

const PendingInfo = styled.View`
  flexDirection    : row;
  justifyContent   : center;
  alignItems       : center;
  paddingHorizontal: 10;
  paddingVertical  : 15;
  height           : 100%;
  width            : 100%;
`;

const ErrorOverlay = styled.View`
  position       : absolute;
  top            : 0;
  left           : 0;
  bottom         : 0;
  height         : 100%;
  width          : 100%;
  backgroundColor: #4dbb78;
  ${'' /* borderWidth    : 2.5;
  borderColor    : #E0E2E1; */}
  borderRadius: 10;
  opacity     : 0.9;
`;

const ErrorTitle = styled.Text`
  color     : #fff;
  fontSize  : 14;
  fontFamily: 'Gotham-Bold';
`;

const ErrorDesc = styled.Text`
  color     : #fff;
  fontSize  : 18;
  fontFamily: 'Gotham-Bold';
`;

const FailDesc = styled.View`
  flexDirection : row;
  justifyContent: center;
  alignItems    : center;
  width         : 75%;
`;

const FailInfo = styled.View`
  flexDirection: row;
  alignItems   : center;
  height       : 100%;
  width        : 100%;
`;

const FailInfoContainer = styled.View`
  paddingLeft: 10;
`;

const FailButton = styled.TouchableHighlight`
  opacity                : 0.7;
  height                 : 100%;
  width                  : 25%;
  justifyContent         : center;
  alignItems             : center;
  alignSelf              : flex-end;
  backgroundColor        : #812c32;
  borderBottomRightRadius: 10;
  borderTopRightRadius   : 10;
  zIndex                 : 30;
`;

const styles = StyleSheet.create({
  active: {
    borderColor  : '#4A90E2',
    shadowColor  : '#000000',
    shadowOpacity: 0.3,
    shadowOffset : {
      width : 0,
      height: 2,
    },
    shadowRadius: 4,
    elevation   : 1,
  },
  progressBar: {
    transform: [{scaleX: -1}]
  }
})

class ProductCard extends Component {
  // async _loadAssetsAsync() {
  // }

  constructor(props) {
    super(props);
    this.state = {
      category      : null,
      isReady       : true,
      hasItem       : true,
      productImage  : null,
      daysLeft      : 0,
      percentage    : 0,
      totalDays     : 0,
      type          : null,
      warrantyPeriod: null,

      itemId        : null,
      categoryId    : null,
      title         : null,
      purchaseDate  : null,
      warrantyPeriod: null,
      image         : null,
      category      : null,
      percentage    : 0,
      daysLeft      : 0,
      totalDays     : 0,
      type          : null,
      productImage  : null,
    };
  }

  componentDidMount() {
    this.setState({
      itemId        : this.props.itemId,
      categoryId    : this.props.categoryId,
      title         : this.props.title,
      purchaseDate  : this.props.purchaseDate,
      warrantyPeriod: null,
      image         : this.props.productImage,
      category      : null,
      percentage    : 0,
      daysLeft      : 0,
      totalDays     : 0,
      type          : this.props.type,
      productImage  : this.props.productImage,
    })

    if (this.props.categoryId) {
      categories.map((item) => {
        if (item.id == this.props.categoryId) {
          this.setState({ category: item });
        }
        return item;
      });
    }

    if (this.props.purchaseDate) {
      // const purchaseDate = moment().subtract(200, 'days');
      const purchaseDate = moment(this.props.purchaseDate);
      let   totalDays    = 0;
      if (this.props.warrantyDays) {
        purchaseDate.add(this.props.warrantyDays, 'days');
        totalDays += this.props.warrantyDays;
      }
      if (this.props.protectionDays) {
        purchaseDate.add(this.props.protectionDays, 'days');
        totalDays += this.props.protectionDays;
      }
      let daysLeft = purchaseDate.diff(moment(), 'days');
      if (daysLeft <= 0) {
        daysLeft = 0;
      }

      this.setState({
        daysLeft,
        totalDays,
        warrantyPeriod: purchaseDate.format('YYYY-MM-DD'),
      });
      if (totalDays > 0) {
        this.setState({
          percentage: Math.round(((totalDays - daysLeft) / totalDays) * 100),
        });
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.warrantyDays != this.props.warrantyDays) {
      if (nextProps.categoryId) {
        categories.map((item) => {
          if (item.id == nextProps.categoryId) {
            this.setState({ category: item });
          }
          return item;
        });
      }

      if (nextProps.purchaseDate) {
        // const purchaseDate = moment().subtract(200, 'days');
        const purchaseDate = moment(nextProps.purchaseDate);
        let   totalDays    = 0;
        if (nextProps.warrantyDays) {
          purchaseDate.add(nextProps.warrantyDays, 'days');
          totalDays += nextProps.warrantyDays;
        }
        if (nextProps.protectionDays) {
          purchaseDate.add(nextProps.protectionDays, 'days');
          totalDays += nextProps.protectionDays;
        }
        let daysLeft = purchaseDate.diff(moment(), 'days');
        if (daysLeft <= 0) {
          daysLeft = 0;
        }

        this.setState({
          daysLeft,
          totalDays,
          warrantyPeriod: purchaseDate.format('YYYY-MM-DD'),
        });
        if (totalDays > 0) {
          this.setState({
            percentage: Math.round(((totalDays - daysLeft) / totalDays) * 100),
          });
        }
      }
    }
  };


  render() {
    const data = {
      itemId        : this.state.itemId,
      categoryId    : this.state.categoryId,
      title         : this.state.title,
      purchaseDate  : this.state.purchaseDate,
      warrantyPeriod: this.state.warrantyPeriod,
      image         : this.state.productImage,
      category      : this.state.category,
      percentage    : this.state.percentage,
      daysLeft      : this.state.daysLeft,
      totalDays     : this.state.totalDays,
      type          : this.state.type,
      productImage  : `https://www.kepp.link/unsecure/getimage/${this.state.productImage}`,
    };

    let categoryIcon = null;

    if (this.state.category) {
      categoryIcon = this.state.category.icon;
    } else if (this.props.type) {
      if (this.props.type === 'Receipt Entry Item' && this.props.status === 'NEW') {
        categoryIcon = require('../../../assets/icons/default/blue/Receipt.png');
      } else if (this.props.type === 'Manual Entry Item') {
        categoryIcon = require('../../../assets/icons/default/blue/Manual-up.png');
      }
    }

    return (
      <CardContainer>
        <TouchableOpacity onPress={() => {this.props.onPress(data)}}>
          <ProductContainer style={(this.props.active) ? styles.active : null}>
            <ProgressContainer>
              <View style={styles.progressBar}>
                <AnimatedCircularProgress
                  rotation        = {0}
                  arcSweepAngle   = {360}
                  size            = {80}
                  width           = {4}
                  fill            = {this.state.percentage}
                  linecap         = "round"
                  tintColor       = "#4A90E2"
                  backgroundColor = "#E0E2E1"
                />
              </View>
              <ProgressCategoryImage
                source = {categoryIcon}
              />
            </ProgressContainer>
            <InfoContainer>
              <ItemTitle>{this.props.title}</ItemTitle>
              <ItemDuration>{this.state.daysLeft} days left</ItemDuration>
            </InfoContainer>
          </ProductContainer>
        </TouchableOpacity>
        {
          this.props.status === 'NEW' && this.props.type === 'Receipt Entry Item' ? 
          <ErrorContainer>
            <ErrorOverlay></ErrorOverlay>
            <PendingInfo>
              <ProgressContainer>
                <ProgressCategoryImage
                  source = {require('../../../assets/icons/default/white/Receipt.png')}
                  style  = {{ width: 45,height: 45 }}
                />
              </ProgressContainer>
              <InfoContainer>
                <ErrorTitle>STATUS</ErrorTitle>
                <ErrorDesc>PENDING RECEIPT{'\n'}VERIFICATION</ErrorDesc>
              </InfoContainer>
            </PendingInfo>
          </ErrorContainer>: this.props.status === 'REJECTED' ?
          <ErrorContainer>
            <ErrorOverlay style={{ backgroundColor: '#da5b5e' }}></ErrorOverlay>
            <FailInfo>
              <FailDesc>
                <Icon
                  name  = 'exclamation'
                  type  = 'font-awesome'
                  size  = {45}
                  color = '#fff'
                />
                <FailInfoContainer>
                  <ErrorTitle>STATUS</ErrorTitle>
                  <ErrorDesc>UPLOAD FAILED</ErrorDesc>
                </FailInfoContainer>
              </FailDesc>
              <FailButton onPress={() => {}}>
                <Icon
                  name  = 'arrow-right'
                  type  = 'feather'
                  size  = {32}
                  color = '#fff'
                />
              </FailButton>
            </FailInfo>
          </ErrorContainer>: null
        }
      </CardContainer>
    );
  }
}

export default ProductCard;