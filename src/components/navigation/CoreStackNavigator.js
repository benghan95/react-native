import { StackNavigator } from 'react-navigation';

import LandingScreen from '../screens/LandingScreen';
import LoginScreen from '../screens/LoginScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import HomeScreen from '../screens/HomeScreen';
import MenuScreen from '../screens/MenuScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import UploadMethodScreen from '../screens/UploadMethodScreen';
import QRCodeScreen from '../screens/QRCodeScreen';
import ScanReceiptScreen from '../screens/ScanReceiptScreen';
import ManualUploadScreen from '../screens/ManualUploadScreen';
import UploadSuccessScreen from '../screens/UploadSuccessScreen';
import ManualUploadSuccessScreen from '../screens/ManualUploadSuccessScreen';
import ContactUsScreen from '../screens/ContactUsScreen';

const CoreStackNavigator = StackNavigator(
  {
    Landing            : { screen: LandingScreen },
    Login              : { screen: LoginScreen },
    Welcome            : { screen: WelcomeScreen },
    Home               : { screen: HomeScreen },
    Menu               : { screen: MenuScreen },
    ProductDetails     : { screen: ProductDetailsScreen },
    UploadMethod       : { screen: UploadMethodScreen },
    QRCode             : { screen: QRCodeScreen },
    ScanReceipt        : { screen: ScanReceiptScreen },
    ManualUpload       : { screen: ManualUploadScreen },
    UploadSuccess      : { screen: UploadSuccessScreen },
    ManualUploadSuccess: { screen: ManualUploadSuccessScreen },
    ContactUs          : { screen: ContactUsScreen },
  },
  {
    initialRouteName: 'Landing',
    headerMode      : 'none',
  },
);

export default CoreStackNavigator;
