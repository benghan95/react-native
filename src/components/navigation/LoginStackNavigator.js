import React from 'react';
import { StackNavigator } from 'react-navigation';
import LandingScreen from '../screens/LandingScreen';
import LoginScreen from '../screens/LoginScreen';
import WelcomeScreen from '../screens/WelcomeScreen';

const LoginStackNavigator = StackNavigator(
  {
    Landing: { screen: LandingScreen },
    Login  : { screen: LoginScreen },
    Welcome: { screen: WelcomeScreen },
  },
  {
    initialRouteName: 'Landing',
    headerMode      : 'none',
  },
);

export default LoginStackNavigator;