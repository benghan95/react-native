import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, Text, TextInput, Alert, AsyncStorage, Dimensions, PermissionsAndroid, Linking } from 'react-native';
import { Button, Icon, FormInput } from 'react-native-elements';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import CloseButton from '../common/CloseButton';
import Loader from '../common/Loader';
import * as Progress from 'react-native-progress';
import ImageView from 'react-native-image-view';
import PickerSelect from 'react-native-picker-select';
import moment from 'moment';
import Switch from 'react-native-switch-pro';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Actions } from 'react-native-router-flux';
import CalendarEvents from 'react-native-calendar-events';
import MapView, { Marker } from 'react-native-maps';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex             : 1;
  backgroundColor  : #fff;
  paddingTop       : 50;
  paddingBottom    : 15;
  paddingHorizontal: 20;
  position         : relative;
`;

const VerifiedContainer = styled.View`
  alignItems    : center;
  justifyContent: center;
  marginTop     : -15;
  marginBottom  : 20;
`;

const VerifiedText = styled.Text`
  color     : #4A81C0;
  fontFamily: 'Gotham-Bold';
  fontSize  : 13;
  marginTop : 5;
`;

const WarrantyContainer = styled.View`
  alignItems  : center;
  marginBottom: 15;
  position    : relative;
`;

const ExtendNowContainer = styled.View`
  alignItems    : center;
  justifyContent: center;
  marginBottom  : 15;
  position      : absolute;
  top           : 35;
`;

const WarrantyTitle = styled.Text`
  color       : #0D0D0D;
  fontFamily  : 'Gotham-Book';
  fontSize    : 14;
  opacity     : 0.5;
  marginBottom: 10;
`;

const WarrantyDaysLeft = styled.Text`
  color       : #498CDA;
  fontFamily  : 'Gotham-Book';
  fontSize    : 48;
  marginBottom: 5;
`;

const WarrantySubtitle = styled.Text`
  color     : #0D0D0D;
  fontFamily: 'Gotham-Book';
  fontSize  : 14;
  marginTop : -15;
  opacity   : 0.5;
`;

const ProgressContainer = styled.View`
  paddingHorizontal: 5;
`;

const DurationContainer = styled.View`
  justifyContent: space-between;
  flexDirection : row;
  marginTop     : 10;
`;

const DateContainer = styled.View`
`;

const YearText = styled.Text`
  color     : #797979;
  fontFamily: 'Gotham-Book';
  fontSize  : 24;
`;

const DateText = styled.Text`
  color     : #797979;
  fontFamily: 'Gotham-Book';
  fontSize  : 12;
  marginTop : -5;
`;

const DetailsContainer = styled.View`
  paddingTop: 30;
`;

const ItemContainer = styled.View`
  borderBottomWidth: 1;
  borderBottomColor: #D6D6D6;
  paddingVertical  : 30;
`;

const DataContainer = styled.View`
  justifyContent: center;
`;

const DataLabel = styled.Text`
  color       : #4D4D4F;
  fontFamily  : 'Gotham-Book';
  fontSize    : 15;
  marginBottom: 5;
  opacity     : 0.5;
  textAlign   : center;
`;

const DataValue = styled.Text`
  color     : #4D4D4F;
  fontFamily: 'Gotham-Book';
  fontSize  : 20;
  textAlign : center;
`;

const RMText = styled.Text`
  fontSize: 14;
`;

const ActionsContainer = styled.View`
  marginTop   : 25;
  marginBottom: 0;
`;

const ActionButton = styled.TouchableHighlight`
  backgroundColor  : #139AD6;
  borderColor      : #fff;
  borderWidth      : 1;
  paddingHorizontal: 20;
  ${'' /* paddingVertical  : 45; */}
  height        : 120;
  justifyContent: center;
  alignItems    : center;
`;

const ReminderContainer = styled.View`
  backgroundColor  : ${Colors.Default.DeepGreen};
  borderColor      : #fff;
  paddingHorizontal: 20;
  ${'' /* paddingVertical  : 45; */}
  height        : 80;
  flexDirection : row;
  justifyContent: space-between;
  alignItems    : center;
  width         : 100%;
`;

const ActionText = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Bold';
  fontSize  : 16;
  lineHeight: 17;
  textAlign : center;
`;

const SmallNote = styled.Text`
  color     : #ccc;
  fontFamily: 'Gotham-Bold';
  fontSize  : 13;
  lineHeight: 17;
  textAlign : center;
`;

const ClaimHeader = styled.View`
  ${'' /* alignItems: center; */}
  position: relative;
`;

const ClaimTitle = styled.Text`
  color       : #333;
  fontFamily  : 'Gotham-Bold';
  fontSize    : 24;
  lineHeight  : 28;
  textAlign   : center;
  marginBottom: 30;
`;

const ClaimFormContainer = styled.View`
  paddingBottom: 150;
`;

const ClaimLabel = styled.Text`
  color       : #333;
  fontFamily  : 'Gotham-Book';
  fontSize    : 16;
  textAlign   : center;
  marginBottom: 10;
`;

const ProductActionsContainer = styled.View`
  flexDirection : row;
  justifyContent: space-around;
  alignItems    : center;
  marginBottom  : 30;
  marginTop     : 30;
`;

const ProductActionContainer = styled.View`
  justifyContent: center;
  alignItems    : center;
  width         : 120;
`;

const ProductActionText = styled.Text`
  color     : #3A3A3A;
  fontFamily: 'Gotham-Bold';
  fontSize  : 15;
  textAlign : center;
  marginTop : 15;
`;

const CalendarTimeLabel = styled.Text`
  color       : #DEDEDE;
  fontFamily  : 'Gotham-Bold';
  fontSize    : 16;
  textAlign   : center;
  marginBottom: 0;
`;

const CalendarContainer = styled.View`
  paddingHorizontal: 15;
  alignItems       : center;
  justifyContent   : center;
  backgroundColor  : ${Colors.Default.DeepGreen};
`;

const ChangesSuccessContainer = styled.View`
  alignItems     : center;
  justifyContent : center;
  backgroundColor: ${Colors.Default.DeepGreen};
`;

const ChangesSuccessText = styled.Text`
  color          : #fff;
  fontFamily     : 'Gotham-Book';
  textAlign      : center;
  paddingVertical: 10
  fontSize       : 16
`;

const MapContainer = styled.View`
  height          : 500;
  alignItems      : center;
  width           : ${width};
  marginHorizontal: -20;
  marginTop       : 20;
  marginBottom    : -15;
`;

const styles = StyleSheet.create({
  input: {
    color     : '#333',
    fontFamily: 'Gotham-Book',
    fontSize  : 18,
    lineHeight: 20,
    ...Platform.select({
      android: {
        paddingBottom: 15,
      },
    }),
    width: '100%',
  },
  inputContainer: {
    fontSize         : 18,
    fontFamily       : 'Gotham-Book',
    borderWidth      : 1,
    borderColor      : '#999',
    paddingHorizontal: 10,
    paddingTop       : 5,
    paddingBottom    : 5,
    marginBottom     : 30,
    marginHorizontal : 15,
    shadowColor      : '#000000',
    shadowOpacity    : 0.1,
    shadowOffset     : {
      width : 0,
      height: 1,
    },
    shadowRadius: 2,
    elevation   : 1,
    // height      : 40,

    // borderBottomColor: 'rgba(0, 0, 0, 0.5)',
    // marginTop        : 10,
    // marginBottom     : 25,
    // marginLeft       : 20,
    // marginRight      : 20,
    // ...Platform.select({
    //   marginLeft : 0,
    //   marginRight: 0,
    // }),
    // paddingVertical  : 0,
    // paddingHorizontal: 5,
  },
  buttonContainer: {
    backgroundColor: Colors.Default.Cyan,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'Gotham-Bold',
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
});

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady           : true,
      loading           : false,
      isImageViewVisible: false,
      receiptImage      : null,
      productData       : null,
      toClaim           : false,
      failureType       : '',
      failureDesc       : '',
      failureDescOnFocus: false,
      receiptImages     : [],
      isSwitchOn        : false,
      toSupport         : false,
      markedDate        : {
        [moment().format('YYYY-MM-DD')]: {selected: true, selectedColor: '#1299D5'}
      },
      isDateTimePickerVisible: false,
      selectedTime           : '12:00 PM',
      setReminderSuccess     : false,
      latitude               : null,
      longitude              : null,
      userMarker             : null,
      markers                : null,
    };
  }

  componentDidMount() {
    if (this.props.entry) {
      let receiptImages = [];
      if (this.props.entry.receiptImage) {
        receiptImages.push({
          images: { uri: `https://www.kepp.link/unsecure/getimage/${this.props.entry.receiptImage}` },
          title: 'Receipt',
        })
      }

      if (this.props.entry.warrantyCardImage) {
        receiptImages.push({
          images: { uri: `https://www.kepp.link/unsecure/getimage/${this.props.entry.warrantyCardImage}` },
          title: 'Warranty Card',
        })
      }
      this.setState({ receiptImages });
    }

    if (this.props.data) {
      this.setState({
        productData: this.props.data,
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.entry) {
      let receiptImages = [];
      if (nextProps.entry.receiptImage) {
        receiptImages.push({
          images: { uri: `https://www.kepp.link/unsecure/getimage/${nextProps.entry.receiptImage}` },
          title: 'Receipt',
        })
      }

      if (nextProps.entry.warrantyCardImage) {
        receiptImages.push({
          images: { uri: `https://www.kepp.link/unsecure/getimage/${nextProps.entry.warrantyCardImage}` },
          title: 'Warranty Card',
        })
      }

      this.setState({ receiptImages });
    }

    if (nextProps.data) {
      this.setState({
        productData: nextProps.data,
      });
    }
  };

  _extendNow = () => {
    this.props.switchScene('protection');
  }

  _claimWarranty = async (entry) => {
    this.setState({ loading: true });

    if (!this.state.failureType) {
      Alert.alert('Please enter type of your product failure.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.failureDesc) {
      Alert.alert('Please enter a short description / some symptoms on your product failure.');
      this.setState({ loading: false });
      return false;
    }

    const token = await AsyncStorage.getItem('Kepp.jwtToken');
    const data  = {
      itemId     : entry.itemId,
      failureType: this.state.failureType,
      failureDesc: this.state.failureDesc,
    };

    fetch('https://www.kepp.link/secure/claim', {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if (responseJson.statusResp) {
          const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
          if (responseCode >= 100000 && responseCode < 200000) {
            Alert.alert('Claim request has been submitted, please check your email inbox on the status.', '', [
              {
                text   : 'OK',
                onPress: () => {
                  this.setState({
                    toClaim    : false,
                    failureType: null,
                    failureDesc: null,
                  });
                },
              },
            ]);
          } else {
            Alert.alert(responseJson.statusResp.responseDesc);
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _editItem = (entry, data) => {
    Actions.EditDetails({data});
  }

  _deleteItem = (entry) => {
    Alert.alert(
      'Delete Item',
      'Are you sure you want to delete this item?\n(Once deleted it will not be able to revert back)',
      [
        {
          text   : 'Confirm Delete',
          onPress: async () => {
            // this.setState({ loading: true });
            const token = await AsyncStorage.getItem('Kepp.jwtToken');
            fetch(`https://www.kepp.link/secure/entries/${entry.itemId}`, {
              method : 'DELETE',
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
              .then(response => response.json())
              .then((responseJson) => {
                const responseCode = parseInt(responseJson.responseCode, 10);
                if (responseCode >= 100000 && responseCode < 200000) {
                  Alert.alert('Item has been deleted.', null, [
                    {
                      text   : 'OK',
                      onPress: () => {
                        Actions.Home();
                        this.setState({ loading: false });
                      }
                    }
                  ]);
                } else {
                  Alert.alert('Error', responseJson.responseDesc, null, [
                    {
                      text   : 'OK',
                      onPress: () => {
                        Actions.Home();
                        this.setState({ loading: false });
                      }
                    }
                  ]);
                }
              })
              .catch((err) => {
              });
          },
        },
        {
          text: 'Cancel',
        },
      ],
    );
  }

  _switch = () =>{
    this.setState(previousState => {
      return {
        isSwitchOn: !previousState.isSwitchOn,
      }
    })
  }

  _setMarkDates = (day) =>{
    this.setState({
      markedDate: {
        [day.dateString]: {selected: true, selectedColor: '#1299D5'}
      }
    });
  }

  _handleDatePicked = (date) => {
    this.setState({
      selectedTime: moment(date).format('hh:MM A')
    });
    this._hideDateTimePicker();
  };

  _setReminder = () => {
    this.setState({ loading: true });
    CalendarEvents.authorizeEventStore().then((res) => {
      if (res == 'authorized') {
        const date     = Object.keys(this.state.markedDate)[0];
        const time     = this.state.selectedTime;
        const datetime = moment(`${date} ${time}`, 'YYYY-MM-DD hh:mm A');
        const startISO = datetime.toISOString();
        const endISO   = datetime.toISOString();
        const details  = {
          title    : `${this.props.entry.itemShortName} Maintenance Reminder`,
          startDate: startISO,
          endDate  : endISO,
          alarms   : [
            {
              date: 10
            },
            {
              date: 30
            },
            {
              date: 60
            },
            {
              date: 1440
            },
          ]
        }
        this.setState({ loading: false });
        try {
          this.setState({ loading: true });
          CalendarEvents.saveEvent(`${this.props.entry.itemShortName} Maintenance Reminder`, details).then((response) => {
            this.setState({ loading: false });
            if (response) {
              this.setState({
                setReminderSuccess: true
              });
            }
          });
        } catch (err) {
          this.setState({ loading: false });
        }
      } else {
        Alert.alert('Permission denied', 'KEPP needs access to your calendar so that we can set a reminder for you.', [
          {
            text   : 'OK',
            onPress: () => {
              this.setState({ loading: false });
            },
          },
        ]);
      }
    })
  }

  _getSupport = async () => {
    try {
      const token = await AsyncStorage.getItem('Kepp.jwtToken');

      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'KEPP Location Permission',
          'message': 'KEPP needs access to your location so that we can find you the nearest support.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        navigator.geolocation.getCurrentPosition((res) => {
          this.setState({
            region: {
              latitude      : res.coords.latitude,
              longitude     : res.coords.longitude,
              latitudeDelta : 0.0922,
              longitudeDelta: 0.0421,
            },
            // userMarker: {
            //   title      : `Your Location`,
            //   latlng : {
            //     latitude : res.coords.latitude,
            //     longitude: res.coords.longitude,
            //   },
            //   pinColor: '#000'
            // }
          })

          const purchaseLocation = this.props.entry.purchaseLocation;
          // const purchaseLocation = 'TBM';
          fetch(`https://www.kepp.link/secure/shops?chainstoregroup=${purchaseLocation}`, {
            method : 'GET',
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
            .then(response => response.json())
            .then((responseJson) => {
              const outlets = responseJson;
              if (outlets.length > 0) {
                const markers = [];
                outlets.forEach((outlet) => {
                  markers.push({
                    id         : outlet.id,
                    title      : `${outlet.chainStoreGroup} - ${outlet.chainStoreBranch}`,
                    description: `${outlet.operatingHours ? 'Operating Hours: ' + outlet.operatingHours : '' } Tel.: ${outlet.phone}`,
                    latlng     : {
                      latitude : outlet.latitude,
                      longitude: outlet.longitude,
                    },
                  })
                });

                this.setState({ markers });
              } else {
                Alert.alert('Sorry', `We are not able to detect any outlets that are similar to your purchased locations. (You have purchased product from ${purchaseLocation})`);
              }
            })
            .catch((err) => {
              this.setState({ loading: false });
              Alert.alert(JSON.stringify(err));
            });
        }, (err) => {
          console.log(err)
        });
      } else {
        Alert.alert('Error', 'KEPP needs access to your location so that we can find you the nearest support.', [
          {
            text   : 'OK',
            onPress: () => {
              this.setState({ loading: false });
            },
          },
        ])
      }
    } catch (err) {
      console.warn(err)
    }
    this.setState({toSupport: true})
  }

  _visitWebsite = () => {
    // const url = `https://maps.google.com/maps?daddr=${latlng.latitude},${latlng.longitude}`;
    // Linking.canOpenURL(url).then(supported => {
    //   if (supported) {
    //     Platform.select({
    //       ios: () => {
    //         Linking.openURL(`https://maps.apple.com/maps?daddr=${latlng.latitude},${latlng.longitude}`);
    //       },
    //       android: () => {
    //         Linking.openURL(`https://maps.google.com/maps?daddr=${latlng.latitude},${latlng.longitude}`);
    //       }
    //     });
    //   }
    // });
  }

  _openMap = (latlng) => {
    let url = `https://maps.google.com/maps?daddr=${latlng.latitude},${latlng.longitude}`

    if (Platform.OS === 'ios') {
      url = `https://maps.apple.com/maps?daddr=${latlng.latitude},${latlng.longitude}`;
    }

    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        if (Platform.OS === 'ios') {
          Linking.openURL(`https://maps.apple.com/maps?daddr=${latlng.latitude},${latlng.longitude}`);
        } else {
          Linking.openURL(`https://maps.google.com/maps?daddr=${latlng.latitude},${latlng.longitude}`);
        }
      }
    });
  }

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  render() {
    if (this.state.toClaim) {
      return (
        <Container>
          <Loader loading={this.state.loading} />
          <ClaimHeader>
            <Icon
              name           = 'arrow-left'
              type           = 'feather'
              size           = {32}
              color          = '#333'
              containerStyle = {{ position: 'absolute', zIndex: 5, left: -10, top: -15 }}
              iconStyle      = {{ padding: 10 }}
              onPress        = {() => this.setState({ toClaim: false })}
            />
            <ClaimTitle>CLAIM WARRANTY</ClaimTitle>
          </ClaimHeader>
          <ClaimFormContainer>
            <View>
              <ClaimLabel>Failure Type</ClaimLabel>
              <FormInput
                ref                   = {(c) => { this.failureTypeInput = c; }}
                value                 = {this.state.failureType}
                onChangeText          = {failureType => this.setState({ failureType })}
                placeholder           = "Parts not functioning"
                placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                containerStyle        = {styles.inputContainer}
                inputStyle            = {styles.input}
                returnKeyType         = "next"
                underlineColorAndroid = "#fff"
                onSubmitEditing       = {() => this.failureDescInput.focus()}
              />
            </View>
            <View>
              <ClaimLabel>Description</ClaimLabel>
              <FormInput
                multiline
                maxLength             = {256}
                ref                   = {(c) => { this.failureDescInput = c; }}
                value                 = {this.state.failureDesc}
                onChangeText          = {failureDesc => this.setState({ failureDesc })}
                placeholder           = "Tell us more..."
                placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                containerStyle        = {[styles.inputContainer, {height: 250}]}
                inputStyle            = {styles.input}
                returnKeyType         = "next"
                underlineColorAndroid = "#fff"
                onSubmitEditing       = {() => this.failureDescInput.focus()}
                onFocus               = {() => this.setState({ failureDescOnFocus: true })}
              />
            </View>
            <Button
              onPress         = {() => this._claimWarranty(this.props.entry)}
              buttonStyle     = {styles.buttonContainer}
              textStyle       = {styles.buttonText}
              title           = "SUBMIT CLAIM"
              backgroundColor = {Colors.Default.Cyan}
            />
          </ClaimFormContainer>
        </Container>
      );
    } else if (this.state.toSupport) {
      return (
        <Container>
          <Loader loading={this.state.loading} />
          <ClaimHeader>
            <Icon
              name           = 'arrow-left'
              type           = 'feather'
              size           = {32}
              color          = '#333'
              containerStyle = {{ position: 'absolute', zIndex: 5, left: -10, top: -15 }}
              iconStyle      = {{ padding: 10 }}
              onPress        = {() => this.setState({ toSupport: false })}
            />
            <ClaimTitle>INFORMATION &{'\n'}SUPPORT</ClaimTitle>
          </ClaimHeader>
          {/* <Button
            onPress         = {() => this._visitWebsite(this.props.entry)}
            buttonStyle     = {[styles.buttonContainer]}
            textStyle       = {styles.buttonText}
            title           = "VISIT PRODUCT WEBSITE"
            backgroundColor = {Colors.Default.Cyan}
          /> */}
          <MapContainer>
            <MapView
              style = {{
                width: '100%',
                height: '100%',
              }}
              initialRegion={{
                latitude      : 4.210484,
                longitude     : 101.975766,
                latitudeDelta : 0.0922,
                longitudeDelta: 0.0421,
              }}
              showsUserLocation={true}
              region={this.state.region}
            >
              {
                this.state.markers ? this.state.markers.map(marker => (
                  <Marker
                    key         = {marker.id}
                    title       = {marker.title}
                    coordinate  = {marker.latlng}
                    description = {marker.description}
                    // onPress     = {() => this._openMap(marker.latlng)}
                    onCalloutPress = {() => this._openMap(marker.latlng)}
                  />
                )) : null
              }
              {
                this.state.userMarker ? 
                <Marker
                  title      = {this.state.userMarker.title}
                  coordinate = {this.state.userMarker.latlng}
                  pinColor   = {this.state.userMarker.pinColor}
                /> : null
              }
            </MapView>
          </MapContainer>
        </Container>
      );
    }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        {
          this.props.entry ? this.props.entry.status === 'VERIFIED' ? <VerifiedContainer>
            <Image
              source = {require('./../../../assets/icons/default/blue/Verified.png')}
              style  = {{alignSelf: 'center',width: 80, height: 80, resizeMode: 'contain'}}
            />
            <VerifiedText>VERIFIED</VerifiedText>
          </VerifiedContainer> : null : null
        }
        <WarrantyContainer>
          <WarrantyTitle>Warranty Period</WarrantyTitle>
          <WarrantyDaysLeft>{this.state.productData ? this.state.productData.daysLeft : null}</WarrantyDaysLeft>
          <WarrantySubtitle>days left</WarrantySubtitle>

          <ExtendNowContainer>
            <TouchableOpacity
              onPress={() => this._extendNow()}
            >
              <Image
                source = {require('./../../../assets/images/extend-now.png')}
                style  = {{alignSelf: 'center', marginLeft: 200, width: 100, height: 30, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          </ExtendNowContainer>
        </WarrantyContainer>
        <ProgressContainer>
          <Progress.Bar
            color         = "#047CED"
            progress      = {this.state.productData ? (this.state.productData.percentage / 100) : 0}
            height        = {4}
            width         = {null}
            borderRadius  = {0}
            unfilledColor = "#D8D8D8"
            borderWidth   = {0}
          />
          <DurationContainer>
            <DateContainer>
              <YearText>{ this.props.entry ? moment(this.props.entry.purchaseDate).format('YYYY') : '-'}</YearText>
              <DateText>{ this.props.entry ? moment(this.props.entry.purchaseDate).format('DD MMMM') : '-'}</DateText>
            </DateContainer>
            <DateContainer style={{alignItems: 'flex-end'}}>
              <YearText>{ this.state.productData ? (this.state.productData.warrantyPeriod ? moment(this.state.productData.warrantyPeriod).format('YYYY') : '-') : '-'}</YearText>
              <DateText>{ this.state.productData ? (this.state.productData.warrantyPeriod ? moment(this.state.productData.warrantyPeriod).format('DD MMMM') : '-') : '-'}</DateText>
            </DateContainer>
          </DurationContainer>
        </ProgressContainer>
        <DetailsContainer>
          <ItemContainer style={{ flexDirection: 'row' }}>
            <DataContainer style={{ width: '50%', borderRightWidth: 1, borderRightColor: '#D6D6D6' }}>
              <DataLabel style={{ fontSize: 13, }}>Price</DataLabel>
              <DataValue>
                { this.props.entry ? <RMText>RM</RMText> : null }
                <Text style={{ fontSize: 30 }}>{ this.props.entry ? this.props.entry.purchasePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '-'}</Text>
              </DataValue>
            </DataContainer>
            <DataContainer style={{ width: '50%' }}>
              <DataLabel style={{ fontSize: 13, }}>Current Value</DataLabel>
              <DataValue>
                {/* <RMText>RM</RMText> */}
                <Text style={{ fontSize: 30, }}>-</Text>
              </DataValue>
            </DataContainer>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Date of Purchase</DataLabel>
            <DataValue>{ this.state.productData ? moment(this.state.productData.warrantyPeriod).format('DD-MM-YYYY') : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Model</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.model : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Series no.</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.serialNo : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer style={{ borderBottomWidth: 0 }}>
           <DataLabel>Purchase from</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.purchaseLocation : '-'}</DataValue>
          </ItemContainer>
        </DetailsContainer>
        {
          this.state.receiptImages.length > 0 ?
            <Button
              onPress     = {() => this.setState({ isImageViewVisible: true})}
              buttonStyle = {{
                borderWidth      : 1,
                borderColor      : '#979797',
                backgroundColor  : 'transparent',
                paddingTop       : 17,
                paddingVertical  : 15,
                paddingHorizontal: 20,
              }}
              containerViewStyle = {{ marginLeft: 0, marginRight: 0 }}
              textStyle          = {{ fontFamily: 'Gotham-Bold', fontSize: 14 }}
              title              = "VIEW PROOF OF PURCHASE"
              color              = "#5E5E5E"
            /> : null
        }
        {
          this.state.receiptImages.length > 0 ?
          <ImageView
            images       = {this.state.receiptImages}
            imageIndex   = {0}
            isVisible    = {this.state.isImageViewVisible}
            renderFooter = {(currentImage) => { return(<View><Text>{currentImage.title}</Text></View>) }}
          /> : null
        }
        {/* <ReceiptContainer>
          <CloseButton onPress={() => this.props.navigation.navigate('UploadMethod')} />
          <Image
            source = {this.state.entry ? { uri: `https://www.kepp.link/unsecure/getimage/${this.state.entry.receiptImage}` } : null}
            style  = {{ backgroundColor: '#ccc', width: '100%', height: '100%', resizeMode: 'contain' }}
          />
        </ReceiptContainer> */}

        <ActionsContainer>
          <View>
            <ActionButton style={{ paddingHorizontal: 0, height: 80 }}>
              <ReminderContainer>
                <ActionText style={{ textAlign: 'left' }}>MAINTENANCE{'\n'}REMINDER</ActionText>
                <View
                  style = {{ backgroundColor: '#fff', paddingHorizontal: 5, paddingVertical: 5, borderColor: '#979797', borderWidth: 1, borderRadius: 30,  width: 78, height: 37 }}>
                  <Switch
                    defaultValue = {false}
                    value        = {this.state.isSwitchOn}
                    width        = {68}
                    height       = {27}
                    // circleStyle  = {{width: 25, height: 25.}}
                    backgroundActive    = {'transparent'}
                    backgroundInactive  = {'transparent'}
                    circleColorActive   = {'#4A90E2'}
                    circleColorInactive = {'#B5B5B5'}
                    onSyncPress         = {() => this._switch()}
                  />
                </View>
              </ReminderContainer>
            </ActionButton>
          </View>
          {
            this.state.isSwitchOn && !this.state.setReminderSuccess ? 
            <CalendarContainer>
              <Calendar
                onDayPress = {(day) => this._setMarkDates(day)}
                minDate    = {moment().format('YYYY-MM-DD')}
                style      = {{
                  marginTop    : 15,
                  marginBottom : 20,
                  width        : '100%',
                  borderRadius : 10,
                  paddingTop   : 10,
                  paddingBottom: 20,
                }}
                theme={{
                  selectedDayBackgroundColor: '#1299D5',
                  arrowColor: '#BFBFBF',
                  monthTextColor: '#6B7897',
                  textDayFontFamily      : 'Gotham-Bold',
                  textMonthFontFamily    : 'Gotham-Bold',
                  textDayHeaderFontFamily: 'Gotham-Bold',
                  textMonthFontWeight    : 'bold',
                  textDayFontWeight      : 'bold',
                  textDayFontSize        : 14,
                  textMonthFontSize      : 16,
                  textDayHeaderFontSize  : 12,
                }}
                markedDates = {this.state.markedDate}
              />
              <CalendarTimeLabel>TIME</CalendarTimeLabel>
              <View style = {{ flexDirection: 'row', alignItems:'center', justifyContent:'center', paddingBottom:20, position: 'relative'}}>
                <TouchableOpacity onPress = {this._showDateTimePicker} touch>
                  <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 36, color: '#fff', marginRight:10, marginBottom: 10,}}>{this.state.selectedTime}</Text>
                </TouchableOpacity>
                <DateTimePicker
                  mode      = 'time'
                  onConfirm = {this._handleDatePicked}
                  onCancel  = {this._hideDateTimePicker}
                  isVisible = {this.state.isDateTimePickerVisible}
                  titleIOS  = 'Select Time'
                  is24Hour  = {false}
                />
                <Icon
                  name      = 'sort'
                  type      = 'font-awesome'
                  color     = '#DEDEDE'
                  onPress   = {this._showDateTimePicker}
                  component = { TouchableOpacity }
                  containerStyle = {{ position: 'absolute', right: -20, top: 6 }}
                />
              </View>
              <Button
                title              = 'SET REMINDER'
                containerViewStyle = {{ width:' 100%', marginBottom: 15 }}
                buttonStyle        = {{backgroundColor:'#FFF', paddingVertical: 18}}
                textStyle          = {{color:'#5E5E5E', fontFamily: 'Gotham-Bold', fontSize: 18}}
                onPress = {() => this._setReminder()}
              />
            </CalendarContainer>
            : null
          }
          {
            this.state.isSwitchOn && this.state.setReminderSuccess ? 
            <ChangesSuccessContainer>
              <View style = {{paddingTop:50,paddingBottom:80}}>
                <Icon
                  name  = 'check'
                  color = '#FFF'
                  size  = {50}
                  type  = 'octicon'
                  />
                <ChangesSuccessText>Maintenance Reminder{'\n'}has been set.</ChangesSuccessText>
              </View>
            </ChangesSuccessContainer>: null
          }
          {
            this.state.productData ? (this.state.productData.type === 'Receipt Entry Item' ?
              <View style={{ flexDirection: 'row' }}>
                <ActionButton
                  style         = {{ width: '50%' }}
                  onPress       = {() => this.setState({ toClaim: true })}
                  underlayColor = "#1FA5EE"
                >
                  <ActionText>CLAIM{'\n'}WARRANTY</ActionText>
                </ActionButton>
                <ActionButton
                  style         = {{ width: '50%', opacity: 0.8, }}
                  underlayColor = "#1FA5EE"
                >
                  <ActionText>EXTRA{'\n'}PROTECTION{'\n'}<SmallNote>COMING SOON</SmallNote></ActionText>
                </ActionButton>
              </View> : null) : null
          }
          {
            this.props.entry ? this.props.entry.status === 'VERIFIED' ? <View>
              <ActionButton
                onPress       = {() => this._getSupport()}
                underlayColor = "#1FA5EE"
              >
                <ActionText>INFORMATION{'\n'}& SUPPORT</ActionText>
              </ActionButton>
            </View> : null : null
          }
        </ActionsContainer>

        <ProductActionsContainer>
          <ProductActionContainer>
            <TouchableOpacity
              style   = {{ paddingVertical: 15, paddingHorizontal: 25 }}
              onPress = {() => this._editItem(this.props.entry, this.props.data)}
            >
              <Image
                source = {require('./../../../assets/icons/edit-icon.png')}
                style  = {{ alignSelf: 'center', width: 30, height: 30, resizeMode: 'contain' }}
              />
              <ProductActionText>EDIT</ProductActionText>
            </TouchableOpacity>
          </ProductActionContainer>
          <ProductActionContainer>
            <TouchableOpacity
              style   = {{ paddingVertical: 15, paddingHorizontal: 25 }}
              onPress = {() => this._deleteItem(this.props.entry)}
            >
              <Image
                source = {require('./../../../assets/icons/delete-icon.png')}
                style  = {{ alignSelf: 'center', width: 30, height: 30, resizeMode: 'contain' }}
              />
              <ProductActionText>DELETE</ProductActionText>
            </TouchableOpacity>
          </ProductActionContainer>
        </ProductActionsContainer>
      </Container>
    );
  }
}

export default Scene;