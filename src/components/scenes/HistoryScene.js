
import React, { Component } from 'react';
import { Platform, ScrollView, TouchableOpacity, TouchableHighlight, Text, TextInput, Alert, AsyncStorage, Dimensions, Linking, RefreshControl } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Loader from '../common/Loader';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  ${'' /* flex             : 1; */}
  backgroundColor  : #fff;
  ${'' /* paddingVertical  : 20; */}
  paddingBottom    : 700;
  paddingHorizontal: 0;
  position         : relative;
`;

const EntriesContainer = styled.ScrollView`
`;

const EntryContainer = styled.View`
  borderWidth: 1;
  borderColor: #db0f1b;
  flexDirection: row;
  ${'' /* justifyContent: center; */}
  alignItems: center;
  position: relative;
  marginTop: 15;
  marginBottom: 15;
  marginLeft: 20;
  marginRight: 20;
`;

const DescContainer = styled.View`
  borderRightWidth: 2;
  borderRightColor: #999;
  paddingHorizontal: 10;
  paddingVertical: 5;
  marginVertical: 15;
`;

const Date = styled.Text`
  color: #000;
  fontSize: 14;
  fontFamily: 'Gotham-Bold';
  marginBottom: 2;
`;

const Time = styled.Text`
  color: #aaa;
  fontSize: 12;
  fontFamily: 'Gotham-Bold';
`;

const NameContainer = styled.View`
  paddingHorizontal: 10;
`;

const Name = styled.Text`
  color: #000;
  fontSize: 16;
  fontFamily: 'Gotham-Bold';
`;

const EditContainer = styled.View`
  ${'' /* alignSelf: flex-end; */}
  position: absolute;
  right: 15;
`;

const DoneLabelContainer = styled.View`
  backgroundColor: ${Colors.Default.Cyan};
  borderRadius: 30;
  justifyContent: center;
  alignItems: center;
  width: 30;
  height: 30;
  position: absolute;
  right: -15;
  top: -15;
`;

const DoneLabel = styled.View`
  justifyContent: center;
  alignItems: center;
`;


const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.Default.Cyan,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'Gotham-Bold',
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
});

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
      refreshing: false,
      entries: [],
    };
  }

  componentDidMount() {
    this._retrieveEntry();
  }

  _editEntry = (itemId) => {
    Actions.AdminEdit({itemId});
  }

  _onRefresh = () => {
    this._retrieveEntry();
  }

  _retrieveEntry = async () => {
    // this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    fetch(`https://www.kepp.link/secure/entries/`, {
      method : 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        let entries = [];
        responseJson.forEach((entry) => {
          if ((entry.status == 'VERIFIED' || entry.status == 'REJECTED') && entry.users.length > 0 && entry.type === 'Receipt Entry Item') {
            entries.push(entry);
          }
        })
        this.setState({ entries: entries });
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  render() {
    return (
      <Container>
        <Loader loading={this.state.loading} />
        <EntriesContainer
          refreshControl        = {
            <RefreshControl
              refreshing = {this.state.refreshing}
              onRefresh  = {this._onRefresh.bind(this)}
            />
          }
        >
          <View style={{marginTop: 20, marginBottom: 20  }}>
            {
              this.state.entries.map((entry, key) => {
                return (
                  <TouchableOpacity
                    key     = {entry.itemId}
                    onPress = {() => this._editEntry(entry.itemId)}
                  >
                    <EntryContainer>
                      <DescContainer>
                        <Date>03 Aug 2018</Date>
                        <Time>12:00PM</Time>
                      </DescContainer>
                      <NameContainer>
                        <Name>{entry.users[0].firstName}</Name>
                      </NameContainer>
                      <EditContainer>
                        <Icon
                          name  = "edit-2"
                          type  = "feather"
                          color = "#666"
                          size  = {26}
                          onPress={() => this._editEntry(entry.itemId)}
                        />
                      </EditContainer>
                      {
                        entry.status === 'VERIFIED' ? <DoneLabelContainer>
                          <DoneLabel>
                            <Icon
                              name  = "check"
                              type  = "feather"
                              color = "#fff"
                              size  = {24}
                            />
                          </DoneLabel>
                        </DoneLabelContainer> : <DoneLabelContainer style={{ backgroundColor: Colors.Default.Red }}>
                          <DoneLabel>
                            <Icon
                              name  = "x"
                              type  = "feather"
                              color = "#fff"
                              size  = {24}
                            />
                          </DoneLabel>
                        </DoneLabelContainer>
                      }
                    </EntryContainer>
                  </TouchableOpacity>
                );
              })
            }
          </View>
        </EntriesContainer>
      </Container>
    );
  }
}

export default Scene;