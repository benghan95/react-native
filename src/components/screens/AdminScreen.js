import React, { Component } from 'react';
import { Alert, Animated, Platform, TouchableOpacity, Text, StyleSheet, View, Image, ScrollView, Dimensions, AsyncStorage, RefreshControl } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Header from '../common/CustomHeader';
import Loader from '../common/Loader';
import EntriesScene from '../scenes/EntriesScene';
import HistoryScene from '../scenes/HistoryScene';
import Tabs from 'react-native-tabs';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const TabLabel = styled.Text`
  color     : #fff;
  fontSize  : 18;
  fontFamily: 'Gotham-Bold';
`;

const HeaderContainer = styled.View`
  paddingHorizontal: 30;
  paddingTop: 20;
`;

const LogoContainer = styled.View`
  marginBottom: 20;
`;

const WelcomeBack = styled.Text`
  color     : #999;
  fontSize  : 18;
  fontFamily: 'Gotham-Bold';
`;

const WelcomeName = styled.Text`
  color     : #000;
  fontSize  : 32;
  fontFamily: 'Gotham-Bold';
`;

const ContentContainer = styled.View`
  paddingTop: 20;
  position: relative;
`;

const styles = StyleSheet.create({
  tab: {
    backgroundColor  : '#b7b7b7',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    height           : 60,
    // top              : 0,
  },
  icon: {
    height: 60,
  },
  activeLabel: {
    backgroundColor  : `${Colors.Default.Cyan}`,
    marginBottom     : -1,
  },
})

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady   : true,
      refreshing: false,
      page      : 'entries',
      loading   : false,
      entry     : null,
      data      : null,
      isAdmin   : false,
    };
  }

  async componentWillMount() {
    this.setState({ loading: true });
    try {
      const name = await AsyncStorage.getItem('Kepp.name');
      if (name){
        this.setState({ loading: false });
        this.setState({name})
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }

    this.setState({
      page: 'entries',
    });
  }

  componentDidMount() {
    
  }

  _onRefresh = () => {
    this._retrieveEntry();
  }

  _switchScene = (scene) => {
    this.setState({
      page: scene,
    });
  }

  render() {
    return (
      <Container>
        <Loader loading={this.state.loading} />
        <View>
          <Header type="default" bgColor={Colors.Default.Cyan} navigation={this.props.navigation} leftComponent outerContainerStyles={{zIndex: 999, position: 'relative'}} />
          <HeaderContainer>
            <LogoContainer>
              <Image
                source = {require('./../../../assets/images/logo-contactUs.png')}
                style  = {{width: 60, height: 60, resizeMode: 'contain'}}
              />
            </LogoContainer>
            <WelcomeBack>WELCOME BACK,</WelcomeBack>
            <WelcomeName>{this.state.name ? this.state.name.toUpperCase() : null}</WelcomeName>
          </HeaderContainer>
          <ContentContainer>
            <View style={{ height: 60 }}>
              <Tabs
                selected          = {this.state.page}
                style             = {styles.tab}
                iconStyle         = {styles.icon}
                selectedIconStyle = {styles.activeLabel}
                onSelect          = {el=>this.setState({page:el.props.name})}
              >
                <TabLabel name="entries">NEW ENTRIES</TabLabel>
                <TabLabel name="history">HISTORY</TabLabel>
              </Tabs>
            </View>
            {
              this.state.page === 'entries' ? <EntriesScene/> : null
            }
            {
              this.state.page === 'history' ? <HistoryScene/> : null
            }
          </ContentContainer>
        </View>
      </Container>
    );
  }
}

export default Screen;
