import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, AsyncStorage, Dimensions, TextInput, ScrollView, KeyboardAvoidingView } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Header from '../../components/common/CustomHeader';
import QRCode from 'react-native-qrcode';
import Loader from '../../components/common/Loader';
import CloseButton from '../../components/common/CloseButton';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');

const Container = styled.ScrollView`
  flex           : 1;
  backgroundColor: #fff;
`;

const LogoContainer = styled.View`
  alignItems  : center;
  marginBottom: 20;
`;

const ContentContainer = styled.ScrollView`
  
  ${'' /* alignItems    : center; */}
  ${'' /* justifyContent: center; */}
`;

const Title = styled.Text`
  color            : #333;
  fontFamily       : 'gotham-book';
  fontSize         : 24;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 5;
`;

const Subtitle = styled.Text`
  color            : #333;
  fontFamily       : 'gotham-book';
  fontSize         : 14;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 35;
`;

const styles = StyleSheet.create({
  contentContainer: {
    flex             : 1,
    paddingTop       : 60,
    paddingHorizontal: 10,
    paddingBottom    : 20,
  },
  buttonContainer: {
    backgroundColor: Colors.Default.Cyan,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  buttonIcon: {
    ...Platform.select({
      ios: {
        marginRight: 15,
        marginTop  : -2,
      },
      android: {
        marginRight: 15,
        marginTop  : 0,
      },
    }),
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
});

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
      message: '',
    };
  }

  _sendMessage = async () => {
    this.setState({ loading: true });

    if (!this.state.message) {
      Alert.alert('Warning', 'Please enter your message.', [
        {
          text   : 'OK',
          onPress: () => {
            this.setState({ loading: false });
          },
        },
      ]);
      return false;
    }

    const token = await AsyncStorage.getItem('Kepp.jwtToken');
    const data  = {
      message: this.state.message,
    };

    fetch('https://www.kepp.link/secure/contactus', {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.statusResp) {
          const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
          if (responseCode >= 100000 && responseCode < 200000) {
            Alert.alert('Success!', 'Message has been sent, we will reply you in a while.', [
              {
                text   : 'OK',
                onPress: () => {
                  Actions.pop();
                  this.setState({ loading: false });
                },
              },
            ]);
          } else {
            Alert.alert("Error", responseJson.statusResp.responseDesc, [
              {
                text   : 'OK',
                onPress: () => {
                  this.setState({ loading: false });
                },
              },
            ]);
          }
        } else {
          this.setState({ loading: false });
        }
      })
      .catch((err) => {
        Alert.alert("Error", JSON.stringify(err), [
          {
            text   : 'OK',
            onPress: () => {
              this.setState({ loading: false });
            },
          },
        ]);
      });
  }


  render() {
    return (
      <Container>
        <Loader loading = {this.state.loading} />
        <KeyboardAvoidingView behavior="position">
          <ScrollView contentContainerStyle={styles.contentContainer}>
          <CloseButton blue style={{top: 10, right: 10,}} onPress={() => Actions.pop()} />
            <LogoContainer>
              <Image
                source = {require('./../../../assets/images/logo-contactUs.png')}
                style  = {{width: 45, height: 45, resizeMode: 'contain'}}
              />
            </LogoContainer>
            <Title>CONTACT US</Title>
            <Subtitle>Feel free to drop us a line below!</Subtitle>
            <TextInput
              multiline
              ref          = {(c) => { this.messageInput = c; }}
              placeholder  = "Type your message here..."
              onChangeText = {message => this.setState({ message })}
              style        = {{
                fontSize         : 16,
                fontFamily       : 'gotham-book',
                borderWidth      : 1,
                borderColor      : '#666',
                paddingHorizontal: 15,
                paddingTop       : 15,
                paddingBottom    : 15,
                marginBottom     : 20,
                marginHorizontal : 15,
                shadowColor      : '#000000',
                shadowOpacity    : 0.3,
                shadowOffset     : {
                  width : 0,
                  height: 1,
                },
                shadowRadius: 2,
                elevation   : 1,
                height      : (height - 400),
              }}
            />
            <Button
              onPress        = {() => this._sendMessage()}
              buttonStyle    = {styles.buttonContainer}
              textStyle      = {styles.buttonText}
              title          = "SEND"
              backgroudColor = {Colors.Default.Cyan}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

export default Screen;
