import React, { Component } from 'react';
import { Alert, Animated, Platform, TouchableOpacity, TouchableHighlight, Text, StyleSheet, View, Image, ScrollView, Dimensions, AsyncStorage, RefreshControl } from 'react-native';
import { Button, Icon, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Header from '../common/CustomHeader';
import Loader from '../common/Loader';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import ImagePicker from 'react-native-image-picker';
import { categories } from '../../data/categories';
import { months, years } from '../../data/constants';
import Modal from 'react-native-modal';
import PickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const BannerContainer = styled.View`
  marginTop     : -70;
  justifyContent: center;
  alignItems    : center;
  position      : relative;
  height        : 220;
  width         : 100%;
`;

const UploadContainer = styled.View`
  backgroundColor: #1299D5;
  justifyContent : center;
  alignItems     : center;
  height         : 60;
  width          : 60;
  marginBottom   : 10;
  alignSelf      : center;
`;

const UploadText = styled.Text`
  color        : #3A3A3A;
  fontFamily  : 'Gotham-Bold';
  fontSize    : 16;
  textAlign   : center;
`;

const EditProductContainer = styled.View`
  marginTop: 80;
`;

const ChooseCategoryContainer = styled.View`
  flexDirection    : row;
  alignItems       : center;
  paddingHorizontal: 30;
  paddingVertical  : 30;
`;

const EditContainer = styled.View`
  alignSelf     : flex-end;
  flex          : 2;
  justifyContent: flex-end;
  alignItems    : flex-end;
`;

const CategoryImage = styled.Image`
  height    : 40;
  width     : 50;
  marginRight: 10;
  resizeMode: contain;
  flex: 2;
`;

const CategoryContainer = styled.View`
  flex: 8;
`;

const Label = styled.Text`
  color     : #4D4D4F;
  fontFamily: 'Gotham-Book';
  fontSize  : 11;
`;

const Data = styled.Text`
  color     : #4D4D4F;
  fontFamily: 'Gotham-Bold';
  fontSize  : 18;
  marginTop : 1;
`;

const DetailsContainer = styled.View`

`;

const DetailContainer = styled.View`
  borderTopWidth   : 1;
  borderBottomWidth: 1;
  borderTopColor   : #B4B4B4;
  borderBottomColor: #B4B4B4;
  flexDirection    : row;
  marginTop        : -1;
  paddingHorizontal: 30;
  paddingVertical  : 10;
  alignItems       : center;
`;

const InfoContainer = styled.View`
  flex: 10;
`;

const UploadProofContainer = styled.View`
  paddingHorizontal: 30;
  marginTop        : 30;
  marginBottom     : 20;
  alignItems       : center;
`;

const UploadProofTitleContainer = styled.View`
  flexDirection : row;
  justifyContent: center;
  alignItems    : center;
  marginBottom  : 20;
`;

const UploadProofTitle = styled.Text`
  color     : #5E5E5E;
  fontSize  : 15;
  fontFamily: 'Gotham-Bold';
`;

const UploadImageContainer = styled.View`
  flexDirection : row;
  justifyContent: center;
  alignItems    : center;
`;

const ProofImageContainer = styled.View`
  borderWidth     : 1;
  borderColor     : #979797;
  justifyContent  : center;
  alignItems      : center;
  height          : 130;
  width           : 150;
  marginHorizontal: 5;
`;

const ActionsContainer = styled.View`
  flexDirection : row;
  justifyContent: center;
  alignItems    : center;
  marginBottom  : 50;
`;

const ActionContainer = styled.View`
  justifyContent   : center;
  alignItems       : center;
  width            : 120;
  marginHorizontal: 10;
`;

const ActionText = styled.Text`
  color     : #3A3A3A;
  fontFamily: 'Gotham-Bold';
  fontSize  : 15;
  textAlign : center;
  marginTop : 15;
`;

const ModalFooter = styled.View`
  justifyContent   : center;
  alignItems       : center;
  flexDirection    : row;
  paddingHorizontal: 15;
  paddingTop       : 10;
  paddingBottom    : 15;
`;

const ModalAction = styled.Text`
  color            : #139AD6;
  fontFamily       : 'Gotham-Bold';
  fontSize         : 15;
  textAlign        : center;
  marginHorizontal : 5;
  paddingHorizontal: 10;
  paddingVertical  : 5;
`;

const WarrantyContainer = styled.View`
  justifyContent   : center;
  flexDirection    : row;
  paddingHorizontal: 35;
  marginTop        : 25;
  marginBottom     : 10;
  width            : 100%;
`;

const CategoriesListContainer = styled.View`
  height        : 280;
  marginBottom  : 30;
  justifyContent: center;
  alignItems    : center;
  marginTop     : 20;
`;

const CategoryRow = styled.View`
  flexDirection: row;
  width        : 260;
`;

const CategoryCard = styled.View`
  marginHorizontal: 10;
  width           : 75;
`;

const CategoryCardImage = styled.View`
  borderWidth   : 1;
  borderColor   : #139AD6;
  borderRadius  : 15;
  justifyContent: center;
  alignItems    : center;
  width         : 75;
  height        : 75;
`;

const CategoryLabel = styled.Text`
  color       : #666;
  fontFamily  : 'gotham-book';
  fontSize    : 12;
  textAlign   : center;
  marginTop   : 10;
  marginBottom: 20;
`;



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  progressBar: {
    backgroundColor: '#fff',
    borderRadius   : 80,
    transform      : [{scaleX: -1}],
  },
  tab: {
    backgroundColor  : '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    paddingHorizontal: 5,
  },
  activeLabel: {
    borderBottomWidth: 3,
    borderBottomColor: '#1FA5EE',
    marginBottom     : -1,
  },
  indicator: {
    borderBottomColor: '#1FA5EE',
    borderBottomWidth: 2,
    alignSelf        : 'center',
    // width: '100%',
  },
  input: {
    color     : '#333',
    fontFamily: 'Gotham-Bold',
    fontSize  : 18,
    lineHeight: 20,
    ...Platform.select({
      android: {
        paddingBottom: 15,
      },
    }),
    width: '100%',
  },
  inputContainer: {
    borderBottomColor: 'rgba(0, 0, 0, 0.5)',
    marginTop        : 10,
    marginBottom     : 25,
    marginLeft       : 20,
    marginRight      : 20,
    ...Platform.select({
      marginLeft : 0,
      marginRight: 0,
    }),
    paddingVertical  : 0,
    paddingHorizontal: 5,
  },
  inputError: {
    fontSize  : 12,
    textAlign : 'center',
    fontFamily: 'Gotham-Book',
  },
  formLabel: {
    color      : '#666',
    fontFamily: 'Gotham-Bold',
    fontSize   : 14,
    marginLeft : 0,
    marginRight: 0,
    marginTop  : 15,
  },
  datepicker: {
    marginRight: -8,
    marginVertical: 8,
    width: 40,
  }
  // tabbar: {
  //   paddingHorizontal: 0,
  // }
})

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady         : true,
      refreshing      : false,
      page            : null,
      loading         : false,
      entry           : null,
      data            : null,
      isAdmin         : false,

      itemShortName    : null,
      categoryId       : null,
      productImage     : null,
      receiptImage     : null,
      warrantyCardImage: null,
      purchaseLocation : null,
      purchasePrice    : null,
      purchaseDate     : null,
      serialNo         : null,
      warrantyDuration : 0,
      model            : null,

      itemShortNameTemp    : null,
      categoryIdTemp       : null,
      productImageTemp     : null,
      receiptImageTemp     : null,
      warrantyCardImageTemp: null,
      purchaseLocationTemp : null,
      purchasePriceTemp    : null,
      purchaseDateTemp     : null,
      serialNoTemp         : null,
      warrantyDurationTemp : 0,
      warrantyYearTemp     : 0,
      warrantyMonthTemp    : 0,
      modelTemp            : null,

      productNameErr  : null,
      productImageErr : null,
      productImageId  : null,
      receiptImageId  : null,
      warrantyImageId : null,
      warrantyYear    : 1,
      warrantyMonth   : 0,
      showModal       : false,
      handlingImage   : null,

      showEditItemModal: false,
      showCategory     : false,
      showPickerModal  : false,
      showCategoryModal: false,
    };
  }

  componentDidMount() {
    this._retrieveEntry();
  }

  _onRefresh = () => {
    this._retrieveEntry();
  }

  _retrieveEntry = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    const { data } = this.props;
    this.setState({ data });

    if (data.itemId) {
      fetch(`https://www.kepp.link/secure/entries/${data.itemId}`, {
        method : 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then((responseJson) => {
          this.setState({ loading: false });
          this.setState({ entry: responseJson });
          console.log(this.state.entry);
          if (this.state.entry) {
            this.setState({
              itemShortName      : this.state.entry.itemShortName,
              categoryId         : this.state.entry.categoryId,
              productImageId     : this.state.entry.productImage,
              receiptImageId     : this.state.entry.receiptImage,
              warrantyCardImageId: this.state.entry.warrantyCardImage,
              purchaseLocation   : this.state.entry.purchaseLocation,
              purchasePrice      : this.state.entry.purchasePrice,
              purchaseDate       : this.state.entry.purchaseDate,
              serialNo           : this.state.entry.serialNo,
              warrantyDuration   : this.state.entry.warrantyDuration,
              warrantyYear       : Math.round(this.state.entry.warrantyDuration / 365),
              warrantyMonth      : Math.round(this.state.entry.warrantyDuration % 365 / 30),
              model              : this.state.entry.model,
            })
            if (this.state.productImageId) {
              this.setState({
                productImage : `https://www.kepp.link/unsecure/getimage/${this.state.productImageId}`,
              })
            }
            if (this.state.receiptImageId) {
              this.setState({
                receiptImage : `https://www.kepp.link/unsecure/getimage/${this.state.receiptImageId}`,
              })
            }
            if (this.state.warrantyCardImageId) {
              this.setState({
                warrantyImage : `https://www.kepp.link/unsecure/getimage/${this.state.warrantyCardImageId}`,
              })
            }
          }
        })
        .catch((err) => {
          this.setState({ loading: false });
          Alert.alert(JSON.stringify(err));
        });
    }
  }

  _uploadImage = async (token, formData, type, state) => {
    await fetch(`https://www.kepp.link/secure/${type}`, {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.statusResp) {
          if (parseInt(responseJson.statusResp.responseCode, 10) >= 100000) {
            this.setState({
              [state]: responseJson.imageId,
            });
          } else {
            Alert.alert(responseJson.statusResp.responseCode);
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _getFormData = (state) => {
    const formData = new FormData();
    const uri      = this.state[state];
    const uriArr   = uri.split('/');
    const filename = uriArr[uriArr.length - 1];
    const fileArr  = filename.split('.');
    const ext      = fileArr[fileArr.length - 1];
    formData.append('file', {
      uri,
      name: `${filename}`,
      type: `image/${ext}`,
    });

    return formData;
  }

  _processData = async (token) => {
    if (this.state.productImage) {
      const productImageData = this._getFormData('productImage');
      const productImageId   = await this._uploadImage(token, productImageData, 'uploadreceipt', 'productImageId');
    }
    if (this.state.receiptImage) {
      const receiptImageData = this._getFormData('receiptImage');
      const receiptImageId   = await this._uploadImage(token, receiptImageData, 'uploadreceipt', 'receiptImageId');
    }
    if (this.state.warrantyImage) {
      const warrantyImageData = this._getFormData('warrantyImage');
      const warrantyImageId   = await this._uploadImage(token, warrantyImageData, 'uploadwarrantycard', 'warrantyImageId');
    }
    return this.state;
  }

  _editItem = (param, title) => {
    this.setState({
      showEditItemModal: true,
      modalName        : title,
      modalValue       : param,
      modalTempValue   : `${param}Temp`,
      [`${param}Temp`] : this.state[param].toString(),
    });
  }

  _doneEdit = () => {
    this.setState({
      [this.state.modalValue]: this.state[this.state.modalTempValue],
      showEditItemModal      : false,
    })
  }

  _editCategory = () => {
    this.setState({
      categoryIdTemp  : this.state.categoryId,
      showCategoryModal: true
    });
  }

  _doneCategory = () => {
    this.setState({
      categoryId       : this.state.categoryIdTemp,
      showCategoryModal: false
    });
  }

  _onSelectCategory = (categoryId) => {
    this.setState({ categoryIdTemp: categoryId });
  }

  _editDuration = (duration) => {
    this.setState({
      showPickerModal  : true,
      warrantyYearTemp : this.state.warrantyYear,
      warrantyMonthTemp: this.state.warrantyMonth,
    });
  }

  _calcDuration = () => {
    let duration  = 0;
        duration += this.state.warrantyMonth * 30;
        duration += this.state.warrantyYear * 365;

    this.setState({ warrantyDuration: duration });
  }

  _doneDuration = () => {
    this.setState({
      warrantyYear   : this.state.warrantyYearTemp,
      warrantyMonth  : this.state.warrantyMonthTemp,
      showPickerModal: false,
    });

    this._calcDuration();
  }

  _editDate = () => {
    this.setState({showCategory: true});
  }

  _updateItem = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    if (!this.state.itemShortName) {
      Alert.alert('Please enter your product title.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    // if (!this.state.productImage) {
    //   Alert.alert('Please upload your product image.', null, [{
    //     text   : 'OK',
    //     onPress: () => {
    //       this.setState({ loading: false });
    //     }
    //   }]);
    //   return false;
    // }

    // if (!this.state.receiptImage) {
    //   Alert.alert('Please upload your product receipt.', null, [{
    //     text   : 'OK',
    //     onPress: () => {
    //       this.setState({ loading: false });
    //     }
    //   }]);
    //   return false;
    // }

    // if (!this.state.warrantyImage) {
    //   Alert.alert('Please upload your warranty card.', null, [{
    //     text   : 'OK',
    //     onPress: () => {
    //       this.setState({ loading: false });
    //     }
    //   }]);
    //   return false;
    // }

    if (this.state.warrantyMonth == 0 && this.state.warrantyYear == 0) {
      Alert.alert('Please select the warranty duration.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    if (!this.state.model) {
      Alert.alert('Please enter your product model.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    if (!this.state.purchaseDate) {
      Alert.alert('Please enter your purchase date.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    if (!this.state.purchaseLocation) {
      Alert.alert('Please enter the location that you purchase your product.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    if (!this.state.purchasePrice) {
      Alert.alert('Please enter the price that you purchase your product.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    if (!this.state.serialNo) {
      Alert.alert('Please enter the serial no. that displayed on your product.', null, [{
        text   : 'OK',
        onPress: () => {
          this.setState({ loading: false });
        }
      }]);
      return false;
    }

    const process = await this._processData(token);

    const data = {
      itemId           : this.state.entry.itemId,
      itemShortName    : this.state.itemShortName,
      categoryId       : this.state.categoryId.toString(),
      productImage     : this.state.productImageId,
      receiptImage     : this.state.receiptImageId,
      warrantyCardImage: this.state.warrantyCardImageId,
      purchaseLocation : this.state.purchaseLocation,
      purchasePrice    : this.state.purchasePrice,
      purchaseDate     : moment(this.state.purchaseDate).format('YYYY-MM-DD'),
      serialNo         : this.state.serialNo,
      warrantyDuration : this.state.warrantyDuration,
      model            : this.state.model,
    };

    fetch(`https://www.kepp.link/secure/entries/${this.state.entry.itemId}`, {
      method : 'PUT',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        if ('itemShortName' in responseJson) {
          Alert.alert('Success!', 'The product has been updated successfully.', [
            {
              text: 'OK',
              onPress: () => {
                this.setState({ loading: false });
              }
            }
          ]);
        } else {
          Alert.alert('Error', JSON.stringify(responseJson), [
            {
              text: 'OK',
              onPress: () => {
                this.setState({ loading: false });
              }
            }
          ]);
        }
      })
      .catch((err) => {
        console.log(err);
        Alert.alert('Error', JSON.stringify(err), [
          {
            text: 'OK',
            onPress: () => {
              this.setState({ loading: false });
            }
          }
        ]);
      });
  }

  _deleteItem = (entry) => {
    Alert.alert(
      'Delete Item',
      'Are you sure you want to delete this item?\n(Once deleted it will not be able to revert back)',
      [
        {
          text   : 'Confirm Delete',
          onPress: async () => {
            // this.setState({ loading: true });
            const token = await AsyncStorage.getItem('Kepp.jwtToken');
            fetch(`https://www.kepp.link/secure/entries/${entry.itemId}`, {
              method : 'DELETE',
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
              .then(response => response.json())
              .then((responseJson) => {
                const responseCode = parseInt(responseJson.responseCode, 10);
                if (responseCode >= 100000 && responseCode < 200000) {
                  Alert.alert('Item has been deleted.', null, [
                    {
                      text   : 'OK',
                      onPress: () => {
                        Actions.Home();
                        this.setState({ loading: false });
                      }
                    }
                  ]);
                } else {
                  Alert.alert('Error', responseJson.responseDesc, null, [
                    {
                      text   : 'OK',
                      onPress: () => {
                        Actions.Home();
                        this.setState({ loading: false });
                      }
                    }
                  ]);
                }
              })
              .catch((err) => {
              });
          },
        },
        {
          text: 'Cancel',
        },
      ],
    );
  }

  _pickImage = async (param) => {
    this.setState({
      handlingImage: param,
    });
    const options = {
      quality                     : 0.72,
      maxWidth                    : 480,
      chooseFromLibraryButtonTitle: 'Choose From Library...',
      takePhotoButtonTitle        : 'Take Photo...',
      cancelButtonTitle           : 'Cancel',
      title                       : 'Select Photo',
      storageOptions              : {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (!response.didCancel) {
        this.setState({ [param]: response.uri });
      }
    });
  };

  render() {
    return (
      <Container>
        <Modal
          isVisible       = {this.state.showEditItemModal}
          onBackdropPress = {() => this.setState({ showEditItemModal: false })}
          backdropOpacity = {0.5}
        >
          <View style={{ backgroundColor: '#fff', flexWrap: 'wrap', flexDirection: 'column' }}>
            <FormLabel
              style={styles.formLabel}
            >
              {this.state.modalName}
            </FormLabel>
            <FormInput
              value                 = {this.state[this.state.modalTempValue]}
              onChangeText          = {(value) => this.setState({ [this.state.modalTempValue]: value })}
              // placeholder           = "MY LEICA CAMERA"
              // placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
              containerStyle        = {styles.inputContainer}
              inputStyle            = {styles.input}
              returnKeyType         = "done"
              underlineColorAndroid = "#fff"
            />
            <ModalFooter>
              <TouchableOpacity
                onPress = {() => this.setState({ showEditItemModal: false })}
              >
                <ModalAction style={{ color: '#999' }}>CANCEL</ModalAction>
              </TouchableOpacity>
              <TouchableOpacity
                onPress = {() => this._doneEdit()}
              >
                <ModalAction>DONE</ModalAction>
              </TouchableOpacity>
            </ModalFooter>
          </View>
        </Modal>

        <Modal
          isVisible       = {this.state.showCategoryModal}
          onBackdropPress = {() => this.setState({ showCategoryModal: false })}
          backdropOpacity = {0.5}
        >
          <View style={{ backgroundColor: '#fff', flexWrap: 'wrap', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <FormLabel style={styles.formLabel}>PRODUCT CATEGORIES</FormLabel>
            <CategoriesListContainer>
              <ScrollView
                showsVerticalScrollIndicator
                style                 = {{ height: 280 }}
                indicatorStyle        = "white"
                contentContainerStyle = {{ flexWrap: 'wrap', width: 300, flexDirection: 'row', alignContent: 'center' }}
              >
                {
                  categories.map((prop, key) => {
                    return (
                      <CategoryCard key={key}>
                        <TouchableOpacity
                          onPress = {(categoryId) => this._onSelectCategory(prop.id)}
                        >
                          <CategoryCardImage
                            style = {this.state.categoryIdTemp == prop.id ? { backgroundColor: '#139AD6' } : null}
                            // onPress = {(categoryId) => this._onSelectCategory(prop.id)}
                          >
                            <Image
                              source = { this.state.categoryIdTemp == prop.id ? prop.whiteIcon : prop.icon }
                              style  = {{ width: 40, height: 40, resizeMode: 'contain' }} />
                          </CategoryCardImage>
                          <CategoryLabel>{prop.label}</CategoryLabel>
                        </TouchableOpacity>
                      </CategoryCard>
                    );
                  })
                }
              </ScrollView>
            </CategoriesListContainer>
            <ModalFooter>
              <TouchableOpacity
                onPress = {() => this.setState({ showEditItemModal: false })}
              >
                <ModalAction style={{ color: '#999' }}>CANCEL</ModalAction>
              </TouchableOpacity>
              <TouchableOpacity
                onPress = {() => this._doneCategory()}
              >
                <ModalAction>DONE</ModalAction>
              </TouchableOpacity>
            </ModalFooter>
          </View>
        </Modal>

        <Modal
          isVisible       = {this.state.showPickerModal}
          onBackdropPress = {() => this.setState({ showPickerModal: false })}
          backdropOpacity = {0.5}
        >
          <View style={{ backgroundColor: '#fff', flexWrap: 'wrap', flexDirection: 'column', alignItems: 'center' }}>
            <FormLabel style={[styles.formLabel, {textAlign: 'center'}]}>WARRANTY DURATION</FormLabel>
            <WarrantyContainer>
              <View style={{ marginHorizontal: 10 }}>
                <Text style={{ color: '#333', fontFamily: 'Gotham-Book', fontSize: 16, textAlign: 'center', marginBottom: 5 }}>Years</Text>
                <View style={{ width: 80, position: 'relative', borderWidth: 2, borderColor: '#234E7F' }}>
                  <PickerSelect
                    hideIcon
                    placeholder   = {{}}
                    items         = {years}
                    onValueChange = {(value) => { this.setState({ warrantyYearTemp: value }); }}
                    style         = {{
                      inputIOS: {
                        color            : '#234E7F',
                        fontSize         : 28,
                        fontFamily       : 'Gotham-Bold',
                        paddingTop       : 8,
                        paddingHorizontal: 5,
                        paddingLeft      : 15,
                        paddingBottom    : 12,
                        backgroundColor  : 'white',
                      },
                      inputAndroid: {
                        color            : '#234E7F',
                        fontSize         : 28,
                        fontFamily       : 'Gotham-Bold',
                        paddingTop       : 8,
                        paddingHorizontal: 5,
                        paddingLeft      : 15,
                        paddingBottom    : 12,
                        backgroundColor  : 'white',
                      },
                    }}
                    value = {this.state.warrantyYearTemp}
                    ref   = {(el) => { this.yearPicker = el; }}
                  />
                  <Icon
                    name           = "ios-arrow-down"
                    type           = "ionicon"
                    size           = {24}
                    color          = "#234E7F"
                    containerStyle = {{ position: 'absolute', top: 13, right: 10 }}
                    onPress        = {() => {this.yearPicker.togglePicker();}}
                  />
                </View>
              </View>
              <View style={{ marginHorizontal: 10 }}>
                <Text style={{ color: '#333', fontFamily: 'Gotham-Book', fontSize: 16, textAlign: 'center', marginBottom: 5 }}>Months</Text>
                <View style={{ width: 80, position: 'relative', borderWidth: 2, borderColor: '#234E7F' }}>
                  <PickerSelect
                    hideIcon
                    placeholder   = {{}}
                    items         = {months}
                    onValueChange = {(value) => { this.setState({ warrantyMonthTemp: value }); }}
                    style         = {{
                      inputIOS: {
                        color            : '#234E7F',
                        fontSize         : 28,
                        fontFamily       : 'Gotham-Bold',
                        paddingTop       : 8,
                        paddingHorizontal: 5,
                        paddingLeft      : 15,
                        paddingBottom    : 12,
                        backgroundColor  : 'white',
                      },
                      inputAndroid: {
                        color            : '#234E7F',
                        fontSize         : 28,
                        fontFamily       : 'Gotham-Bold',
                        paddingTop       : 8,
                        paddingHorizontal: 5,
                        paddingLeft      : 15,
                        paddingBottom    : 12,
                        backgroundColor  : 'white',
                      },
                    }}
                    value = {this.state.warrantyMonthTemp}
                    ref   = {(el) => { this.monthPicker = el; }}
                  />
                  <Icon
                    name           = "ios-arrow-down"
                    type           = 'ionicon'
                    size           = {24}
                    color          = "#234E7F"
                    containerStyle = {{ position: 'absolute', top: 13, right: 10 }}
                    onPress        = {() => { this.monthPicker.togglePicker(); }}
                  />
                </View>
              </View>

            </WarrantyContainer>
            <ModalFooter>
              <TouchableOpacity
                onPress = {() => this.setState({ showPickerModal: false })}
              >
                <ModalAction style={{ color: '#999' }}>CANCEL</ModalAction>
              </TouchableOpacity>
              <TouchableOpacity
                onPress = {() => this._doneDuration()}
              >
                <ModalAction>DONE</ModalAction>
              </TouchableOpacity>
            </ModalFooter>
          </View>
        </Modal>

        <Loader loading={this.state.loading} />
        <ScrollView
          refreshControl        = {
            <RefreshControl
              refreshing = {this.state.refreshing}
              onRefresh  = {this._onRefresh.bind(this)}
            />
          }
        >
          <Header type="default" bgColor="transparent" navigation={this.props.navigation} leftComponent outerContainerStyles={{zIndex: 999, position: 'relative'}} />
          <BannerContainer>
            <Image
              source = {this.state.productImage ? { uri: this.state.productImage } : null}
              style  = {{ backgroundColor: '#ccc', width: '100%', height: 220, resizeMode: 'cover' }}
            />
            <View style={{ position: 'absolute', bottom: -75, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableHighlight
                underlayColor = 'transparent'
                activeOpacity = {0.8}
                onPress = {() => this._pickImage('productImage')}
              >
                <View>
                  <UploadContainer>
                    <Image
                      source = {require('./../../../assets/icons/default/white/Upload.png')}
                      style  = {{ height: 25, width: 20, resizeMode: 'contain' }}
                    />
                  </UploadContainer>
                  <UploadText>UPLOAD{'\n'}PICTURE</UploadText>
                </View>
              </TouchableHighlight>
            </View>
          </BannerContainer>

          <EditProductContainer>
            <ChooseCategoryContainer>
              <CategoryImage
                source = {this.state.categoryId ? categories[this.state.categoryId - 1].icon : null}
              />
              <CategoryContainer>
                <Label>Categories</Label>
                <Data>{this.state.categoryId ? categories[this.state.categoryId - 1].label.toUpperCase() : null}</Data>
              </CategoryContainer>
              <EditContainer>
                <Icon
                  name      = 'caret-right'
                  type      = 'font-awesome'
                  color     = '#4D4D4F'
                  component = { TouchableOpacity }
                  iconStyle = {{paddingVertical: 13, paddingRight: 10}}
                  onPress   = { () => this._editCategory() }
                  size      = {24}
                />
              </EditContainer>
            </ChooseCategoryContainer>

            <DetailsContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Product Name</Label>
                  <Data>{ this.state.itemShortName ? this.state.itemShortName.toUpperCase() : null }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'edit-2'
                    type      = 'feather'
                    color     = 'rgba(0, 0, 0, 0.5)'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13}}
                    onPress   = { () => this._editItem('itemShortName', 'PRODUCT NAME') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Price</Label>
                  <Data>{ this.state.purchasePrice ? `RM ${this.state.purchasePrice}` : null }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'edit-2'
                    type      = 'feather'
                    color     = 'rgba(0, 0, 0, 0.5)'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13}}
                    onPress   = { () => this._editItem('purchasePrice', 'PRICE') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Warranty Period</Label>
                  <Data>{ this.state.warrantyDuration ? `${this.state.warrantyYear ? `${this.state.warrantyYear} Year(s) ` : '' }${this.state.warrantyMonth ? `${this.state.warrantyMonth} Month(s) ` : '' }` : '-' }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'sort'
                    type      = 'font-awesome'
                    color     = '#4D4D4F'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13, paddingRight: 5}}
                    onPress   = { () => this._editDuration('warrantyDuration') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Date of Purchase</Label>
                  <Data>{ this.state.purchaseDate ? moment(this.state.purchaseDate).format('DD.MM.YYYY') : '-' }</Data>
                </InfoContainer>
                <EditContainer>
                  <DatePicker
                    ref            = {(c) => { this.purchaseDatePicker = c; }}
                    style          = {styles.datepicker}
                    mode           = 'date'
                    placeholder    = '2015-12-31'
                    format         = 'YYYY-MM-DD'
                    showIcon       = {true}
                    confirmBtnText = "Confirm"
                    cancelBtnText  = "Cancel"
                    hideText = {true}
                    iconSource = {require('../../../assets/icons/calendar-icon.png')}
                    customStyles   = {{
                      dateIcon: {
                        resizeMode: 'contain',
                        width     : 25,
                        height    : 25,
                      }
                    }}
                    onDateChange = {purchaseDate => this.setState({ purchaseDate })}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Model</Label>
                  <Data>{ this.state.model ? this.state.model.toUpperCase() : '-' }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'edit-2'
                    type      = 'feather'
                    color     = 'rgba(0, 0, 0, 0.5)'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13}}
                    onPress   = { () => this._editItem('model', 'PRODUCT MODEL') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Serial No.</Label>
                  <Data>{ this.state.serialNo ? this.state.serialNo : '-' }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'edit-2'
                    type      = 'feather'
                    color     = 'rgba(0, 0, 0, 0.5)'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13}}
                    onPress   = { () => this._editItem('serialNo', 'SERIAL NO.') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>

              <DetailContainer>
                <InfoContainer>
                  <Label>Purchased from</Label>
                  <Data>{ this.state.purchaseLocation ? this.state.purchaseLocation : '-' }</Data>
                </InfoContainer>
                <EditContainer>
                  <Icon
                    name      = 'edit-2'
                    type      = 'feather'
                    color     = 'rgba(0, 0, 0, 0.5)'
                    component = { TouchableOpacity }
                    iconStyle = {{paddingVertical: 13}}
                    onPress   = { () => this._editItem('purchaseLocation', 'PURCHASE LOCATION') }
                    size      = {24}
                  />
                </EditContainer>
              </DetailContainer>
            </DetailsContainer>

            <UploadProofContainer>
              <UploadProofTitleContainer>
                <Image
                  source = {require('./../../../assets/icons/receipt-icon.png')}
                  style  = {{ height: 20, width: 20, marginRight: 10, resizeMode: 'contain' }}
                />
                <UploadProofTitle>UPLOAD PROOF OF PURCHASE</UploadProofTitle>
              </UploadProofTitleContainer>
              <UploadImageContainer>
                <TouchableOpacity
                  onPress   = { () => this._pickImage('receiptImage') }
                >
                  <ProofImageContainer>
                    {
                      this.state.receiptImage ? <Image
                        source  = {{ uri: this.state.receiptImage }}
                        style   = {{ width: '100%', height: '100%', resizeMode: 'cover' }}
                        onPress = {() => this._pickImage('receiptImage')}
                      /> :
                      <Icon
                        name      = 'plus'
                        type      = 'entypo'
                        color     = '#868686'
                        component = { TouchableOpacity }
                        iconStyle = {{paddingVertical: 13}}
                        onPress   = { () => this._pickImage('receiptImage') }
                        size      = {36}
                      />
                    }
                  </ProofImageContainer>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress   = { () => this._pickImage('warrantyImage') }
                >
                  <ProofImageContainer>
                    {
                      this.state.warrantyImage ? <Image
                        source  = {{ uri: this.state.warrantyImage }}
                        style   = {{ width: '100%', height: '100%', resizeMode: 'cover' }}
                        onPress = {() => this._pickImage('warrantyImage')}
                      /> :
                      <Icon
                        name      = 'plus'
                        type      = 'entypo'
                        color     = '#868686'
                        component = { TouchableOpacity }
                        iconStyle = {{paddingVertical: 13}}
                        onPress   = { () => this._pickImage('warrantyImage') }
                        size      = {36}
                      />
                    }
                  </ProofImageContainer>
                </TouchableOpacity>
              </UploadImageContainer>
            </UploadProofContainer>

            <ActionsContainer>
              <ActionContainer>
                <TouchableOpacity
                  style   = {{ paddingVertical: 15, paddingHorizontal: 25 }}
                  onPress = {() => this._updateItem(this.state.entry)}
                >
                  <Image
                    source = {require('./../../../assets/icons/done-icon.png')}
                    style  = {{ alignSelf: 'center', width: 30, height: 30, resizeMode: 'contain' }}
                  />
                  <ActionText>UPDATE{'\n'}INFO</ActionText>
                </TouchableOpacity>
              </ActionContainer>
              <ActionContainer>
                <TouchableOpacity
                  style   = {{ paddingVertical: 15, paddingHorizontal: 25 }}
                  onPress = {() => this._deleteItem(this.state.entry)}
                >
                  <Image
                    source = {require('./../../../assets/icons/delete-icon.png')}
                    style  = {{ alignSelf: 'center', width: 30, height: 30, resizeMode: 'contain' }}
                  />
                  <ActionText>DELETE</ActionText>
                </TouchableOpacity>
              </ActionContainer>
            </ActionsContainer>

          </EditProductContainer>
        </ScrollView>
      </Container>
    );
  }
}

export default Screen;
