import React, { Component } from 'react';
import { Platform, TouchableOpacity, AsyncStorage, Alert, View, StyleSheet, Image, NetInfo } from 'react-native';
import { Button } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Loader from '../../components/common/Loader';
import { Actions } from 'react-native-router-flux';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const LogoView = styled.View`
  flex      : 4;
  alignItems: center;
  ${'' /* justifyContent: center; */}
  justifyContent: flex-end;
  paddingBottom : 30;
`;

const ActionView = styled.View`
  flex          : 2;
  alignItems    : center;
  justifyContent: flex-start;
`;

const styles = StyleSheet.create({
  getStartedIcon: {
    color: '#139AD6',
    ...Platform.select({
      ios: {
        marginTop  : -3,
        marginRight: 0,
      },
      android: {
        marginTop  : 0,
        marginRight: 0,
      },
    }),
  },
  getStartedText: {
    color     : '#139AD6',
    fontSize  : 24,
    textAlign : 'center',
    fontFamily: 'Gotham-Bold',
  }
})

export default class LandingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
    };
  }

  componentDidMount = () => {
    this.setState({ loading: false });
  };

  _onStart = async () => {
    this.setState({ loading: true });
    try {
      NetInfo.isConnected.fetch().then(async (isConnected) => {
        if (!isConnected) {
          this.setState({ loading: false });
          Alert.alert('Warning', 'KEPP requries internet access to retrieve your warranty entries, please check your internet connection.', [
            {
              text   : 'OK',
              onPress: async () => {
                this.setState({ loading: false });
              },
            },
          ]);
        } else {
          const token = await AsyncStorage.getItem('Kepp.jwtToken');
          if (token) {
            fetch('https://www.kepp.link/secure/userdetail', {
              method : 'GET',
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
              .then(response => response.json())
              .then(async (responseJson) => {
                this.setState({ loading: false });
                if (responseJson.statusResp) {
                  const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
                  if (responseCode >= 100000 && responseCode < 200000) {
                    await AsyncStorage.setItem('Kepp.name', responseJson.firstName);
                    if (responseJson.isAdmin)
                      await AsyncStorage.setItem('Kepp.isAdmin', 'true');
                    else
                      await AsyncStorage.setItem('Kepp.isAdmin', 'false');
                    Actions.Home();
                  } else {
                    await AsyncStorage.removeItem('Kepp.jwtToken');
                    Actions.Login();
                  }
                } else {
                  await AsyncStorage.removeItem('Kepp.jwtToken');
                  Actions.Login();
                }
              })
              .catch(async () => {
                await AsyncStorage.removeItem('Kepp.jwtToken');
                this.setState({ loading: false });
                Actions.Login();
                // Alert.alert(JSON.stringify(err));
              });
          } else {
            this.setState({ loading: false });
            Actions.Login();
          }
        }
      });
    } catch (err) {
      Alert.alert('Warning', 'Please ensure that your device has internet access.');
    }
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading}/>
        <LogoView>
          <Image
            source = {require('./../../../assets/images/kepp-intro-animation.gif')}
            style  = {{width: 200, height: 335, resizeMode: 'contain' }}
          />
        </LogoView>
        <ActionView>
          <Button
            iconRight   = {{ name: 'arrow-right', type: 'feather', size: 32, style: styles.getStartedIcon }}
            onPress     = {() => this._onStart()}
            buttonStyle = {{backgroundColor: 'transparent'}}
            textStyle   = {styles.getStartedText}
            component   = {TouchableOpacity}
            title       = "GET STARTED"
            color       = "#841584"
          />
        </ActionView>
      </Container>
    );
  }
}