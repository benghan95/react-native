import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, TouchableHighlight, Text, StyleSheet, View, Image, Dimensions, AsyncStorage, Linking } from 'react-native';
import { Button, Icon, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Header from '../common/CustomHeader';
import Swiper from 'react-native-swiper';
import Slider from 'react-native-slider';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import CloseButton from '../common/CloseButton';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: ${Colors.Default.Cyan};
`;

const ContentContainer = styled.View`
  flex           : 1;
  alignItems     : center;
  paddingVertical: 15;
`;

const LogoContainer = styled.View`
  flex          : 2;
  alignItems    : center;
  justifyContent: center;
`;

const MenuContainer = styled.View`
  flex             : 6;
  paddingHorizontal: 30;
  ${'' /* alignItems: center; */}
  ${'' /* justifyContent: center; */}
  width: 100%;
`;

const MenuItem = styled.View`
  marginVertical: 5;
`;

const ItemLink = styled.TouchableOpacity`
  flexDirection  : row;
  paddingVertical: 8;
`;

const ItemText = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Bold';
  fontSize  : 22;
  marginLeft: 20;
  marginTop : 5;
`;

const FooterContainer = styled.View`
  flex          : 2;
  alignItems    : center;
  justifyContent: center;
`;

const ConnectTitle = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Bold';
  fontSize  : 14;
  textAlign : center;
  opacity   : 0.5;
`;

const SocialMediaContainer = styled.View`
  flexDirection: row;
  alignItems   : center;
`;

const Copyright = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Bold';
  fontSize  : 11;
  textAlign : center;
  opacity   : 0.5;
`;

const styles = StyleSheet.create({
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class Screen extends Component {

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    // const fontAssets = cacheFonts([FontAwesome.font]);

    // await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  constructor(props){
    super(props);
    this.state = {
      duration       : 4,
      isReady        : true,
      isAdmin        : false,
      title          : null,
      titleErr       : null,
      productImage   : null,
      productImageErr: null,
      proveImage     : null,
      proveImageErr  : null,
      currentSlide   : 0,
    }
  }

  async componentWillMount() {
    try {
      const isAdmin = await AsyncStorage.getItem('Kepp.isAdmin');
      if (isAdmin === 'true') {
        this.setState({isAdmin: true});
      }
    } catch (err) {
      console.log(err);
    }
  }

  _logout = async () => {
    await AsyncStorage.removeItem('Kepp.jwtToken');
    Actions.Login();
  }

  _socialClick = (url, title) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Actions.Web({title: title, url: url});
      }
    });
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          // startAsync = {this._loadAssetsAsync}
          // onFinish   = {() => this.setState({ isReady: true })}
          // onError    = {console.warn}
        />
      );
    }

    return (
      <Container>
        <ContentContainer>
          <CloseButton
            onPress = {() => Actions.pop()}
            style   = {{ top: 50, right: 20 }}
          />
          <LogoContainer>
            <Image
              source = {require('./../../../assets/images/LOGO-white.png')}
              style  = {{alignSelf: 'center', width: 70, height: 70, resizeMode: 'contain'}}
            />
          </LogoContainer>

          <MenuContainer>
            <MenuItem>
              <ItemLink onPress={() => Actions.Home()}>
                <Icon
                  name  = "list"
                  type  = "entypo"
                  color = "#fff"
                  size  = {26}
                />
                {/* <Image
                  source = {{ uri: 'https://via.placeholder.com/20x20'}}
                  style  = {{alignSelf: 'center', width: 20, height: 20}}
                /> */}
                <ItemText>MY ITEM</ItemText>
              </ItemLink>
            </MenuItem>
            <MenuItem>
              <ItemLink onPress={() => Actions.UploadMethod()}>
                <Icon
                  name  = 'upload-cloud'
                  type  = 'feather'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>UPLOAD</ItemText>
              </ItemLink>
            </MenuItem>
            {/* <MenuItem>
              <ItemLink onPress={() => this.props.navigation.navigate('Home')}>
                <Icon
                  name  = 'circle-with-plus'
                  type  = 'entypo'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>XTRA PROTECTION</ItemText>
              </ItemLink>
            </MenuItem> */}
            {/* <MenuItem>
              <ItemLink onPress={() => this.props.navigation.navigate('Home')}>
                <Icon
                  name  = 'user'
                  type  = 'entypo'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>PROFILE</ItemText>
              </ItemLink>
            </MenuItem> */}
            <MenuItem>
              <ItemLink onPress={() => Actions.ContactUs()}>
                <Icon
                  name  = 'phone'
                  type  = 'entypo'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>CONTACT US</ItemText>
              </ItemLink>
            </MenuItem>
            <MenuItem>
              <ItemLink onPress={() => Actions.Admin()}>
                <Icon
                  name  = 'format-list-checks'
                  type  = 'material-community'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>ADMIN</ItemText>
              </ItemLink>
            </MenuItem>
            <MenuItem>
              <ItemLink onPress={() => this._logout()}>
                <Icon
                  name  = 'log-out'
                  type  = 'feather'
                  color = '#fff'
                  size  = {26}
                />
                <ItemText>LOGOUT</ItemText>
              </ItemLink>
            </MenuItem>
          </MenuContainer>

          <FooterContainer>
            <ConnectTitle>Connect with us</ConnectTitle>
            <SocialMediaContainer>
              <Icon
                name           = 'facebook'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this._socialClick('https://www.facebook.com/just.kepp', 'Facebook') }
                size           = {32}
              />
              <Icon
                name           = 'twitter'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this._socialClick('https://twitter.com/just_kepp', 'Twitter') }
                size           = {32}
              />
              <Icon
                name           = 'instagram'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this._socialClick('https://www.instagram.com/just_keep', 'Instagram') }
                size           = {32}
              />
              <Icon
                name           = 'linkedin'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this._socialClick('https://www.linkedin.com/company/kepp', 'LinkedIn') }
                size           = {32}
              />
              {/* <Icon
                name           = 'google-plus'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this.props.navigation.navigate("Home") }
                size           = {32}
              />
              <Icon
                name           = 'instagram'
                type           = 'font-awesome'
                color          = '#fff'
                component      = { TouchableOpacity }
                containerStyle = {{}}
                iconStyle      = {{padding: 15}}
                onPress        = { () => this.props.navigation.navigate("Home") }
                size           = {32}
              /> */}
            </SocialMediaContainer>
            <Copyright>&copy; 2018 Warrantyzen . All Rights Reserved.</Copyright>
          </FooterContainer>
        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;