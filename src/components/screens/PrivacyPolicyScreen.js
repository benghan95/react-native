import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, AsyncStorage, WebView } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Header from '../../components/common/CustomHeader';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const styles = StyleSheet.create({
})

class Screen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
    };
  }

  render() {
    return (
      <Container>
        <Header
          type       = "backOnly"
          bgColor    = {this.state.toggled ? Colors.Default.Cyan : "#234E7F"}
          pageTitle  = "Privacy Policy"
          navigation = {this.props.navigation}
        />
        <WebView
          source = {{uri: 'http://www.kepp.com.my/privacy-policy.html'}}
        />
      </Container>
    );
  }
}

export default Screen;