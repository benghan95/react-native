import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, AsyncStorage, Text, StyleSheet, View, Image, Dimensions } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import styled from 'styled-components';
import Header from '../common/CustomHeader';
import CloseButton from '../common/CloseButton';
import Loader from '../common/Loader';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
  paddingTop    : 70;
`;

const WelcomeTitle = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Book';
  fontSize  : 20;
  marginTop : -70;
  textAlign : center;
`;

const Subtitle = styled.Text`
  color       : #fff;
  fontFamily  : 'Gotham-Book';
  fontSize    : 14;
  textAlign   : center;
  marginBottom: 50;
`;

const ChooseText = styled.Text`
  color       : #fff;
  fontFamily  : 'Gotham-Bold';
  fontSize    : 14;
  textAlign   : center;
  marginBottom: 50;
`;

const MethodsContainer = styled.View`
  flexDirection  : row;
  position       : relative;
  ${'' /* paddingTop     : 50; */}
  backgroundColor: #234E7F;
  zIndex         : 10;
  width          : ${width};
  height         : 300;
  justifyContent : center;
`;

const MethodContainer = styled.View`
  backgroundColor : #234E7F;
  alignItems      : center;
  paddingBottom   : 10;
  width           : 80;
  marginHorizontal: 10;
`;

const MethodTitle = styled.Text`
  color     : #fff;
  fontFamily: 'Gotham-Bold';
  fontSize  : 12;
  lineHeight: 13;
  textAlign : center;
`;

const ScanReceiptInstruction = styled.View`
  width   : 320;
  position: absolute;
  top     : 80;
  zIndex  : 20;
`;


const styles = StyleSheet.create({
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class HomeScreen extends Component {
  state = {
    isReady: true,
  }

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    // const fontAssets = cacheFonts([FontAwesome.font]);

    // await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  constructor(props){
    super(props);
    this.state = {
      loading: false,
      name   : null,
    }
  }

  componentWillMount = async () => {
    this.setState({ loading: true });
    try {
      const name = await AsyncStorage.getItem('Kepp.name');
      if (name){
        this.setState({ loading: false });
        this.setState({name})
      } else {
        const token = await AsyncStorage.getItem('Kepp.jwtToken');
        if (token) {
          fetch('https://www.kepp.link/secure/userdetail', {
            method : 'GET',
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
            .then(response => response.json())
            .then(async (responseJson) => {
              await AsyncStorage.setItem('Kepp.name', responseJson.firstName);
              this.setState({
                loading: false,
                name   : responseJson.firstName
              });
            })
            .catch(async (err) => {
              await AsyncStorage.removeItem('Kepp.jwtToken');
              this.setState({ loading: false });
              Actions.Login();
            });
        } else {
          this.setState({ loading: false });
          Actions.Login();
        }
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  };

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading}/>
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <ContentContainer>
          <CloseButton onPress = {() => Actions.Home()} />
          <WelcomeTitle>Hello{this.state.name ? ` ${this.state.name}` : null},</WelcomeTitle>
          <Subtitle>Let's start organise your warranty</Subtitle>
          <ChooseText>Choose any upload method</ChooseText>
          <MethodsContainer>
            <MethodContainer>
              <TouchableOpacity onPress = {() => Actions.QRCode()}>
                <Image 
                  source = {require('./../../../assets/icons/default/white/QR-CODE.png')}
                  style  = {{alignSelf: 'center', marginBottom: 10, width: 50, height: 50, resizeMode: 'contain'}}
                />
                <MethodTitle>QR{"\n"}CODE</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>
            <MethodContainer style={{ marginTop: -23, marginBottom: 0 }}>
              <TouchableOpacity onPress = {() => Actions.ScanReceipt()}>
                <Image
                  source = {require('./../../../assets/icons/default/white/Receipt-Verify.png')}
                  style  = {{alignSelf: 'center', marginBottom: 3, marginLeft: -10, width: 65, height: 80, resizeMode: 'contain'}}
                />
                <MethodTitle>SCAN{"\n"}RECEIPT</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>
            <MethodContainer>
              <TouchableOpacity onPress = {() => Actions.ManualUpload()}>
                <Image 
                  source = {require('./../../../assets/icons/default/white/Manual-up.png')}
                  style  = {{alignSelf: 'center', marginBottom: 10, width: 50, height: 50, resizeMode: 'contain'}}
                />
                <MethodTitle>MANUAL{"\n"}UPLOAD</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>

            <ScanReceiptInstruction>
              <Image 
                source = {require('./../../../assets/images/scan-receipt-instruction.png')}
                style  = {{alignSelf: 'center', marginLeft: -10, width: 320, resizeMode: 'contain'}}
              />
            </ScanReceiptInstruction>
          </MethodsContainer>
        </ContentContainer>
      </Container>
    );
  }
}

export default HomeScreen;