import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Colors } from '../../constants/styles';
import Header from '../../components/common/CustomHeader';
import CloseButton from '../../components/common/CloseButton';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
  buttonText: {
    color     : '#234E7F',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'Gotham-Bold'
  },
  button: {
    paddingTop   : 18,
    paddingBottom: 16,
    width        : 200,
    marginBottom : 8,
  },
  container: {
    flex           : 1,
    backgroundColor: '#234E7F'
  },
  contentContainer:{
    flex          : 1,
    alignItems    : 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    marginBottom: 50
  },
  title: {
    color            : '#fff',
    fontFamily       : 'Gotham-Book',
    fontSize         : 26,
    textAlign        : 'center',
    paddingHorizontal: 20,
    marginBottom     : 20,
  },
  subtitle:{
    color            : '#fff',
    fontFamily       : 'Gotham-Book',
    fontSize         : 14,
    textAlign        : 'center',
    paddingHorizontal: 20,
    marginBottom     : 70,
  },
  backMainText: {
    color            : '#fff',
    fontFamily       : 'Gotham-Bold',
    fontSize         : 24,
    marginBottom     : 30,
    paddingHorizontal: 20,
    textAlign        : 'center',
  },
  addMoreContainer: {
    opacity       : 0.5,
    alignItems    : 'center',
    justifyContent: 'center',
  },
  CTATitle:{
    color            : '#fff',
    fontFamily       : 'Gotham-Book',
    fontSize         : 12,
    textAlign        : 'center',
    paddingHorizontal: 20,
  }
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class Screen extends Component {
  state = {
    isReady: true,
    receipt: null,
  }

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    // const fontAssets = cacheFonts([FontAwesome.font]);

    // await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  constructor(props){
    super(props);
  }

  _resetUpload = () => {
    this.setState({
      receipt: null,
    })
  }

  _submitReceipt = () => {
    if (!this.state.receipt){
      Alert.alert(
        "Please ensure that you have uploaded your receipt."
      )
    }
  }

  render() {
    let { receipt } = this.state;

    // if (!this.state.isReady) {
    //   return (
    //     // <AppLoading
    //       // startAsync = {this._loadAssetsAsync}
    //       // onFinish   = {() => this.setState({ isReady: true })}
    //       // onError    = {console.warn}
    //     // />
    //   );
    // }

    return (
      <View style = { styles.container }>
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <View style = { styles.contentContainer }>
          <CloseButton onPress = {() => Actions.Home()} />
          <View style = { styles.iconContainer }>
            <Image
              source = {require('./../../../assets/icons/default/white/Done-icon.png')}
              style  = {{alignSelf: 'center', width: 70, height: 70, resizeMode: 'contain'}}
            />
          </View>
          <Text style = { styles.title}>UPLOAD SUCCESS</Text>
          <Text style = { styles.subtitle }>Your record is successfully uploaded and{'\n'}available in next *24 hours after verified.
            {'\n'}
            {'\n'}* working hours excluded Public Holiday,{'\n'}Saturday and Sunday.
          </Text>
          <TouchableOpacity onPress = { () => Actions.Home() }>
            <Text style = { styles.backMainText }>BACK TO MAIN</Text>
          </TouchableOpacity>
          <View style = { styles.addMoreContainer }>
            <Icon
              name           = 'plus'
              type           = 'font-awesome'
              color          = '#fff'
              component      = { TouchableOpacity }
              containerStyle = {{
                borderStyle : 'dotted',
                borderRadius: 100,
                borderWidth : 2,
                borderColor : '#fff',
                marginBottom: 13,
              }}
              iconStyle = {{paddingVertical: 23, paddingHorizontal: 25}}
              onPress   = { () => Actions.UploadMethod() }
              size      = {18}
            />
            <Text style = { styles.CTATitle}>Add More</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default Screen;