export const proofEntries = [
  {
    state: 'proofImage1',
  },
  {
    state: 'proofImage2',
  },
  {
    state: 'proofImage3',
  },
];

export const warrantyEntries = [
  {
    state: 'warrantyImage1',
  },
  {
    state: 'warrantyImage2',
  },
  {
    state: 'warrantyImage3',
  },
];
